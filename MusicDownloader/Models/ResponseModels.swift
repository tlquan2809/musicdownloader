//
//  ResponseModels.swift
//  MusicDownloader
//
//  Created by Quan Tran on 9/29/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import Foundation
import RealmSwift

struct ResponseModel: Codable {
    let headers: HeadersModel
    let results: [Song]
}

struct HeadersModel: Codable {
    let status: String
    let code: Int
    let error_message: String
    let warnings: String
    let results_count: Int
    let next: String
}

class Song: Object, Codable {
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var duration: Int = 0
    @objc dynamic var artist_id: String = ""
    @objc dynamic var artist_name: String = ""
    @objc dynamic var artist_idstr: String = ""
    @objc dynamic var album_name: String = ""
    @objc dynamic var album_id: String = ""
    @objc dynamic var license_ccurl: String = ""
    @objc dynamic var position: Int = 0
    @objc dynamic var releasedate: String = ""
    @objc dynamic var album_image: String = ""
    @objc dynamic var audio: String = ""
    @objc dynamic var audiodownload: String = ""
    @objc dynamic var prourl: String = ""
    @objc dynamic var shorturl: String = ""
    @objc dynamic var shareurl: String = ""
    @objc dynamic var lyrics: String = ""
    @objc dynamic var waveform: String = ""
    @objc dynamic var image: String = ""
    @objc dynamic var isFavorited: Bool = false
    @objc dynamic var localUrl: String = ""
    @objc dynamic var history: Bool = false

    override static func primaryKey() -> String? {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id, name, duration, artist_id, artist_name, artist_idstr, album_name, album_id, license_ccurl, position, releasedate, album_image, audio, audiodownload, prourl, shorturl, shareurl, lyrics, waveform, image
    }
//
//    init(id: String, name: String, duration: Int, artist_id: String, artist_name: String, artist_idstr: String, album_name: String, album_id: String, license_ccurl: String, position: Int, album_image: String, audio: String, audiodownload: String, prourl: String, shorturl: String, shareurl: String, lyrics: String, waveform: String, image: String, isFavorited: Bool, localUrl: String, history: Bool) {
//        self.id = id
//        self.name = name
//        self.duration = duration
//        self.artist_id = artist_id
//        self.artist_name = artist_name
//        self.artist_idstr = artist_idstr
//        self.album_name = album_name
//        self.album_id = album_id
//        self.license_ccurl = license_ccurl
//        self.position = position
//        self.album_image = album_image
//        self.audio = audio
//        self.audiodownload = audiodownload
//        self.prourl = prourl
//        self.shorturl = shorturl
//        self.shareurl = shareurl
//        self.lyrics = lyrics
//        self.waveform = waveform
//        self.image = image
//        self.isFavorited = isFavorited
//        self.localUrl = localUrl
//        self.history = history
//    }
//
//    func encode(with coder: NSCoder) {
//        <#code#>
//    }
//
//    required convenience init(coder: NSCoder) {
//        let id = coder.decodeObject(forKey: CodingKeys.id.rawValue) as! String
//        let name = coder.decodeObject(forKey: CodingKeys.name.rawValue) as! String
//        let duration = coder.decodeObject(forKey: CodingKeys.duration.rawValue) as! Int
//        let artist_id = coder.decodeObject(forKey: CodingKeys.artist_id.rawValue) as! String
//        let artist_name = coder.decodeObject(forKey: CodingKeys.artist_name.rawValue) as! String
//        let artist_idstr = coder.decodeObject(forKey: CodingKeys.artist_idstr.rawValue) as! String
//        let album_name = coder.decodeObject(forKey: CodingKeys.album_name.rawValue) as! String
//        let album_id = coder.decodeObject(forKey: CodingKeys.album_id.rawValue) as! String
//        let license_ccurl = coder.decodeObject(forKey: CodingKeys.license_ccurl.rawValue) as! String
//        let position = coder.decodeObject(forKey: CodingKeys.position.rawValue) as! Int
//        let album_image = coder.decodeObject(forKey: CodingKeys.album_image.rawValue) as! String
//        let audio = coder.decodeObject(forKey: CodingKeys.audio.rawValue) as! String
//        let audiodownload = coder.decodeObject(forKey: CodingKeys.audiodownload.rawValue) as! String
//        let prourl = coder.decodeObject(forKey: CodingKeys.prourl.rawValue) as! String
//        let shorturl = coder.decodeObject(forKey: CodingKeys.shorturl.rawValue) as! String
//        let shareurl = coder.decodeObject(forKey: CodingKeys.shareurl.rawValue) as! String
//        let lyrics = coder.decodeObject(forKey: CodingKeys.lyrics.rawValue) as! String
//        let waveform = coder.decodeObject(forKey: CodingKeys.waveform.rawValue) as! String
//        let image = coder.decodeObject(forKey: CodingKeys.image.rawValue) as! String
//        let isFavorited = coder.decodeBool(forKey: "isFavorited")
//        let localUrl = coder.decodeObject(forKey: "localUrl") as! String
//        let history = coder.decodeBool(forKey: "history")
//        self.init(
//    }
}

class Playlist: Object {
    @objc dynamic var name: String = ""
    dynamic var songs = List<String>()
    
    override static func primaryKey() -> String? {
        return "name"
    }
    
    convenience init(name: String, songs: List<String>) {
        self.init()
        
        self.name = name
        self.songs = songs
    }
}
