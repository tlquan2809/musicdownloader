//
//  View.swift
//  MusicDownloader
//
//  Created by son nguyen on 9/3/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    class func getXIBView() -> Self {
        return Bundle.main.loadNibNamed(String(describing: Self.self), owner: nil, options: nil)?.first as! Self
    }
    
    @discardableResult
    public func addViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        guard let nibView = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            return nil
        }
        addSubview(nibView)
        nibView.boundsToSuperView()
        return nibView
    }

    public func boundsToSuperView() {
        if let superView = self.superview {
            frame = superView.bounds
            autoresizingMask = [.flexibleWidth, .flexibleHeight]
            translatesAutoresizingMaskIntoConstraints = true
        }
    }
}
