//
//  AppDelegate.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/20/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import RxMusicPlayer
import RxSwift
import UserNotifications
import Firebase
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var currentSong: Song?
    var player: RxMusicPlayer?
    var list: [RxMusicPlayerItem] = Array()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        _ = MyMusicViewController.loadFromNib()
        print("documents: \(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0])")
        
        registerForRemoteNotification()
        setupPlayInBackground()
        player = RxMusicPlayer(items: list)!
        AVPlayerSingleton.shared.setupCommandCenter()
        AVPlayerSingleton.shared.song = getCurrentSong()
        return true
    }
    
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
            UIApplication.shared.delegate = self
        }
    }
    
    private func setupPlayInBackground() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            // Set the audio session category, mode, and options.
            try audioSession.setCategory(.playback, mode: .moviePlayback, options: [])
            do {
                try audioSession.setActive(true)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        } catch {
            print("Failed to set audio session category.")
        }
    }
    
    func setCurrentSong(song: Song) {
        currentSong = song
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(song)
            UserDefaults.standard.set(data, forKey: "currentSong")
        } catch {
            print("Unable to Encode Note (\(error))")
        }
    }
    
    private func getCurrentSong() -> Song? {
        if let data = UserDefaults.standard.data(forKey: "currentSong") {
            do {
                let decoder = JSONDecoder()
                let note = try decoder.decode(Song.self, from: data)
                if let localSong = RealmManager.shared.getSongByID(note.id) {
                    note.isFavorited = localSong.isFavorited
                    note.localUrl = localSong.localUrl
                    note.history = localSong.history
                }
                return note
            } catch {
                print("Unable to Decode Note (\(error))")
            }
        }
        return nil
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        guard let player = AVPlayerSingleton.shared.player, AVPlayerSingleton.shared.isPlaying else {
            return
        }
        
        AVPlayerSingleton.shared.isPlaying = false
        player.pause()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        guard let player = AVPlayerSingleton.shared.player, AVPlayerSingleton.shared.isPlaying else {
            return
        }
        
        AVPlayerSingleton.shared.isPlaying = false
        player.pause()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        guard let player = AVPlayerSingleton.shared.player, AVPlayerSingleton.shared.isPlaying else {
            return
        }
        
        AVPlayerSingleton.shared.isPlaying = false
        player.pause()
    }
}
