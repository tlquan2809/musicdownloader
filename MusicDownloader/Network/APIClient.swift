//
//  APIClient.swift
//  MusicDownloader
//
//  Created by Quan Tran on 9/29/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import Foundation
import Alamofire

enum Genres: String {
    case pop, electronic, rock, chillout, classical
    
    var icon: UIImage? {
        switch self {
        case .pop:
            return UIImage(named: "ic_pop")
        case .electronic:
            return UIImage(named: "ic_edm")
        case .rock:
            return UIImage(named: "ic_rock")
        case .chillout:
            return UIImage(named: "ic_chillout")
        case .classical:
            return UIImage(named: "ic_classical")
        }
    }
}

class APIClient {
    static let shared = APIClient()
    private init() {}
    
    var headers = ["Upgrade": "h2,h2c", "Content-Type": "application/x-www-form-urlencoded"]
    var downloadingList: [String] = []
    
    func getRequest(endpoint: String, completion: @escaping (ResponseModel) -> Void, errorHandler: (() -> Void)?) {
        Alamofire.request(baseURL + endpoint).responseJSON(completionHandler: { response in
            guard response.result.isSuccess else {
                errorHandler?()
                return
            }

            let decoder = JSONDecoder()
            guard let data = response.data, let decodedResponse = try? decoder.decode(ResponseModel.self, from: data) else {
                errorHandler?()
                return
            }
            
            decodedResponse.results.forEach({ song in
                if let savedSong = RealmManager.shared.getSongByID(song.id) {
                    song.localUrl = savedSong.localUrl
                    song.isFavorited = savedSong.isFavorited
                    song.history = savedSong.history
                }
            })
            
            completion(decodedResponse)
        })
    }
}

extension APIClient {
    func getPopular(completion: @escaping (ResponseModel) -> Void, errorHandler: (() -> Void)?) {
        let endpoint = "&type=single+albumtrack&order=popularity_total&limit=25&offset=0"
        getRequest(endpoint: endpoint, completion: completion, errorHandler: errorHandler)
    }
    
    func getListened(offset: Int, completion: @escaping (ResponseModel) -> Void, errorHandler: (() -> Void)?) {
        let endpoint = "&type=single+albumtrack&order=listens_total&limit=25&offset=\(offset)"
        getRequest(endpoint: endpoint, completion: completion, errorHandler: errorHandler)
    }
    
    func getDownloads(offset: Int, completion: @escaping (ResponseModel) -> Void, errorHandler: (() -> Void)?) {
        let endpoint = "&type=single+albumtrack&order=downloads_total&limit=25&offset=\(offset)"
        getRequest(endpoint: endpoint, completion: completion, errorHandler: errorHandler)
    }
    
    func getSearch(keyword: String, offset: Int, completion: @escaping (ResponseModel) -> Void, errorHandler: (() -> Void)?) {
        let replacedKeyword = keyword.removeExtraSpaces().replacingOccurrences(of: " ", with: "+")
        let endpoint = "&audiodlformat=mp32&order=relevance&type=single+albumtrack&search=\(replacedKeyword)&limit=25&offset=\(offset)"
        getRequest(endpoint: endpoint, completion: completion, errorHandler: errorHandler)
    }
    
    func getGenres(_ genre: Genres, offset: Int, completion: @escaping (ResponseModel) -> Void, errorHandler: (() -> Void)?) {
        let genreRaw = genre.rawValue
        let endpoint = "&type=single+albumtrack&tags=\(genreRaw)&limit=25&offset=\(offset)"
        getRequest(endpoint: endpoint, completion: completion, errorHandler: errorHandler)
    }
}
