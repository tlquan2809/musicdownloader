//
//  HomeTabbarController.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/31/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class HomeTabbarController: UITabBarController {
    
    @IBOutlet weak var customTabBar: CustomTabBar!
    
    var appDelegate: AppDelegate!
    var playerVC: PopupPlayerVC?
    var playerCenter:CGPoint! = .zero
    var viewCenter:CGPoint! = .zero
    var tabbarDefaultCenter:CGPoint = .zero
    var tabbarEndCenter:CGPoint = .zero
    var gestureSimultaneously = false
    var popupView: TestView?
    private let miniPlayerHeight: CGFloat = 72

    var isFirstShowMiniPlayer: Bool = true
    var isFirstTimeChangeTab: Bool = true
    var isFirstTimeLoad: Bool = true
    
    var isIOS12: Bool {
        return UIDevice.current.systemVersion.contains("12")
    }

    private var transparentView: UIView!
    
    var isIOS11: Bool {
        return UIDevice.current.systemVersion.contains("11")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        setupNotification()
        setupMiniPlayer()
        customTabBar.selectionIndicatorImage = UIImage(named: "exBottomLine")
        
        for item in customTabBar.items! {
            item.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -8)
            item.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 9)], for: .normal)
            item.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 9)], for: .selected)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        rescaleIcons()
    }
    
    private func setupMiniPlayer() {
        let guide = view.safeAreaLayoutGuide
        let height = guide.layoutFrame.size.height
        var bottomSafeAreaHeight: CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows[0]
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            bottomSafeAreaHeight = window.frame.maxY - safeFrame.maxY
        }
        
        let y = height - miniPlayerHeight - tabBar.frame.height - bottomSafeAreaHeight
        popupView = TestView.init(frame: CGRect.init(x: 0, y: y, width: UIScreen.main.bounds.size.width, height: 72))
        view.addSubview(popupView!)
        view.didAddSubview(popupView!)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        popupView!.addGestureRecognizer(tap)
    }
    
    func showTransparentView() {
        transparentView = UIView()
        transparentView.frame = self.view.frame
        self.transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        let countView = view.subviews.count
        self.view.insertSubview(transparentView, at: countView - 1)
    }
    
    func hideTranparentView() {
        guard let transView = transparentView else {
            return
        }
        
        transView.removeFromSuperview()
    }
    
    override var selectedIndex: Int {
        didSet {
//            rescaleForIOS12()
        }
    }
    
    override var selectedViewController: UIViewController? {
        didSet {
//            rescaleForIOS12()
        }
    }
    
    private func rescaleForIOS12() {
        if isFirstTimeLoad {
            isFirstTimeLoad = false
            return
        }
        
        if isIOS12 && isFirstTimeChangeTab {
            isFirstTimeChangeTab = false
            rescaleIcons()
        }
    }
    
    func rescaleIcons() {
        for item in customTabBar.items! {
            item.imageInsets = UIEdgeInsets(top: 74, left: 2, bottom: 2, right: 2)
        }
    }
    
    func setDefaultIcons() {
        for item in customTabBar.items! {
            item.imageInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        }
    }
    
    private func setupNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showMiniPlayerView),
                                               name: NSNotification.Name("showMiniPlayerView"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(removeMiniPlayerView),
                                               name: NSNotification.Name("removeMiniPlayerView"),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func removeMiniPlayerView() {
        customTabBar.isHidden = true
        popupView?.isHidden = true
    }
    
    @objc func showMiniPlayerView() {
        customTabBar.isHidden = false
        popupView?.isHidden = false
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        guard let playerVC = self.storyboard?.instantiateViewController(withIdentifier: "PlayerViewViewController") as? PlayerViewViewController else { return }
        
        playerVC.modalPresentationStyle = .overCurrentContext
        NotificationCenter.default.post(name: NSNotification.Name("removeMiniPlayerView"), object: nil)
        
        guard let selectedViewController = viewControllers?[selectedIndex] else {
            return
        }
        
        selectedViewController.present(playerVC, animated: true, completion: nil)
    }
    
    @objc func handlePLay(_ sender: UITapGestureRecognizer? = nil) {
        
    }
}

extension UIDevice {
    var haveTopNotch: Bool {
        if userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1920, 2208, 1334:
                return false
            case 2436, 2688, 1792:
                return true
            default:
                return false
            }
        }
        return false
    }
}
