//
//  TestView.swift
//  MusicDownloader
//
//  Created by son nguyen on 9/3/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class TestView: UIView {

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblSongName: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var prevView: UIView!
    @IBOutlet weak var playView: UIView!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var playImage: UIImageView!
    
    var appDelegate: AppDelegate!
    var playerSingleton = AVPlayerSingleton.shared
    var dataManager = DataManager.shared
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        addViewFromNib()
        setupTapGesture()
        setupNotification()
        updateUI()
    }
    
    private func setupNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateUI),
                                               name: NSNotification.Name("updateMiniPlayer"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateProgressView),
                                               name: NSNotification.Name("update_progress"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.updateView),
                                               name: NSNotification.Name("updateMiniUIForCurrentSong"),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateProgressView() {
        if AVPlayerSingleton.shared.isEnd() {
            AVPlayerSingleton.shared.playNext()
            return
        }
        guard let song = playerSingleton.song else { return }
        let value = Float(Int(AVPlayerSingleton.shared.timePlayed) * 100 / song.duration)
        progressView.progress = value / 100
    }
    
    private func resetUI() {
        progressView.progress = 0
        let placeholderImage = UIImage(named: "ic_blur")!
        imgCover?.image = placeholderImage
        lblArtistName.isHidden = true
        lblSongName.text = "Not Playing"
        playImage.image = UIImage(named: "ic_play")
    }
    
    @objc func updateUI() {
        guard let song = playerSingleton.song else {
            resetUI()
            return
        }
        lblArtistName.isHidden = false
        let value = Float(Int(AVPlayerSingleton.shared.timePlayed) * 100 / song.duration)
        progressView.progress = value / 100
        let placeholderImage = UIImage(named: "ic_blur")!
        imgCover?.sd_setImage(with: URL(string: song.image), placeholderImage: placeholderImage)
        lblArtistName.text = song.artist_name
        lblSongName.text = song.name
        playImage.image = DataManager.shared.isPlaying ? UIImage(named: "ic_pause") : UIImage(named: "ic_play")
    }
 
    private func setupTapGesture() {
        let prevTapGesture = UITapGestureRecognizer(target: self, action: #selector(handlePrevTap))
        prevView.isUserInteractionEnabled = true
        prevView.addGestureRecognizer(prevTapGesture)
        
        let playTapGesture = UITapGestureRecognizer(target: self, action: #selector(handlePlayTap))
        playView.isUserInteractionEnabled = true
        playView.addGestureRecognizer(playTapGesture)
        
        let nextTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleNextTap))
        nextView.isUserInteractionEnabled = true
        nextView.addGestureRecognizer(nextTapGesture)
    }
    
    @objc func handlePrevTap() {
        if playerSingleton.playPrevious() {
            self.updateView()
        }
    }
    
    @objc func handlePlayTap() {
        guard let song = playerSingleton.song else {
            return
        }
        
        if playerSingleton.player == nil {
            _ = playerSingleton.initPlayer(withSong: song)
        }
        
        if playerSingleton.player?.rate == 1.0 {
            playerSingleton.player?.pause()
            DataManager.shared.isPlaying = false
            let play = UIImage(named: "ic_play")!
            playImage.image = play
        } else {
            playerSingleton.player?.play()
            DataManager.shared.isPlaying = true
            let pause = UIImage(named: "ic_pause")!
            playImage.image = pause
        }
    }
    
    @objc func handleNextTap() {
        if playerSingleton.playNext() {
            self.updateView()
        }
    }
    
    @objc func updateView() {
        guard let song = playerSingleton.song else { return }
        
        let placeholderImage = UIImage(named: "ic_blur")!
        imgCover?.sd_setImage(with: URL(string: song.image), placeholderImage: placeholderImage)
        lblArtistName.text = song.artist_name
        lblSongName.text = song.name
        playImage.image = UIImage(named: "ic_pause")!
        progressView.progress = 0.0
    }
}
