//
//  HomeTabbarController.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/31/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class HomeTabbarController: UITabBarController {
    
    @IBOutlet weak var customTabBar: CustomTabBar!
    
    var appDelegate: AppDelegate!
    var playerVC: PopupPlayerVC?
    var playerCenter:CGPoint! = .zero
    var viewCenter:CGPoint! = .zero
    var tabbarDefaultCenter:CGPoint = .zero
    var tabbarEndCenter:CGPoint = .zero
    var gestureSimultaneously = false
    var popupView: TestView?
    private let miniPlayerHeight: CGFloat = 72

    var isFirstShowMiniPlayer: Bool = true
    var isFirstTimeChangeTab: Bool = true
    var isFirstTimeLoad: Bool = true
    
    var isIOS12: Bool {
        return UIDevice.current.systemVersion.contains("12")
    }

    private var transparentView: UIView!
    
    var isIOS11: Bool {
        return UIDevice.current.systemVersion.contains("11")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        setupNotification()
        
        let guide = view.safeAreaLayoutGuide
        let height = guide.layoutFrame.size.height
        var bottomSafeAreaHeight: CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows[0]
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            bottomSafeAreaHeight = window.frame.maxY - safeFrame.maxY
        }
        
        let y = height - miniPlayerHeight - tabBar.frame.height - bottomSafeAreaHeight
        popupView = TestView.init(frame: CGRect.init(x: 0, y: y, width: UIScreen.main.bounds.size.width, height: 72))
        customTabBar.selectionIndicatorImage = UIImage(named: UIDevice().haveTopNotch ? "notchBottomLine" : "bottomLine")
        
        for item in customTabBar.items! {
            item.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -5)
            item.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)], for: .normal)
            item.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)], for: .selected)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setDefaultIcons()
    }
    
    func showTransparentView() {
        transparentView = UIView()
        transparentView.frame = self.view.frame
        self.transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        let countView = view.subviews.count
        self.view.insertSubview(transparentView, at: countView - 1)
    }
    
    func hideTranparentView() {
        guard let transView = transparentView else {
            return
        }
        
        transView.removeFromSuperview()
    }
    
    override var selectedIndex: Int {
        didSet {
            rescaleForIOS12()
        }
    }
    
    override var selectedViewController: UIViewController? {
        didSet {
            rescaleForIOS12()
        }
    }
    
    private func rescaleForIOS12() {
        if isFirstTimeLoad {
            isFirstTimeLoad = false
            return
        }
        
        if isIOS12 && isFirstTimeChangeTab {
            isFirstTimeChangeTab = false
            rescaleIcons()
        }
    }
    
    func rescaleIcons() {
        for item in customTabBar.items! {
            item.imageInsets = UIEdgeInsets(top: 74, left: 1, bottom: 1, right: 1)
        }
    }
    
    func setDefaultIcons() {
        for item in customTabBar.items! {
            item.imageInsets = UIEdgeInsets(top: 2, left: 1, bottom: 1, right: 1)
        }
    }
    
    private func setupNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showMiniPlayerView),
                                               name: NSNotification.Name("showMiniPlayerView"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(removeMiniPlayerView),
                                               name: NSNotification.Name("removeMiniPlayerView"),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func removeMiniPlayerView() {
        popupView?.removeFromSuperview()
        customTabBar.height = 49
        setDefaultIcons()
        customTabBar.isHidden = true
        customTabBar.selectionIndicatorImage = UIImage(named: "bottomLine")
    }
    
    @objc func showMiniPlayerView() {
        view.addSubview(popupView!)
        view.didAddSubview(popupView!)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        popupView!.addGestureRecognizer(tap)
    
        customTabBar.isHidden = false
        customTabBar.height = 121
        customTabBar.layoutIfNeeded()
        customTabBar.selectionIndicatorImage = UIImage(named: "exBottomLine")
        
        if isFirstShowMiniPlayer {
            isFirstShowMiniPlayer = false
            var targetViewController: UIViewController
            if let navViewController = viewControllers?[selectedIndex] as? UINavigationController {
                targetViewController = navViewController.viewControllers[0]
            } else {
                targetViewController = viewControllers![selectedIndex]
            }
            
            let currentHeight = targetViewController.view.frame.height
            targetViewController.view.frame.size.height = currentHeight - 121
            targetViewController.view.layoutIfNeeded()
        }
        
        if !isIOS12 && !isIOS11 {
            rescaleIcons()
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        guard let playerVC = self.storyboard?.instantiateViewController(withIdentifier: "PlayerViewViewController") as? PlayerViewViewController else { return }
        
        playerVC.modalPresentationStyle = .overCurrentContext
        NotificationCenter.default.post(name: NSNotification.Name("removeMiniPlayerView"), object: nil)
        
        guard let selectedViewController = viewControllers?[selectedIndex] else {
            return
        }
        
        selectedViewController.present(playerVC, animated: true, completion: nil)
    }
    
    @objc func handlePLay(_ sender: UITapGestureRecognizer? = nil) {
        
    }
}

extension UIDevice {
    var haveTopNotch: Bool {
        if userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1920, 2208, 1334:
                return false
            case 2436, 2688, 1792:
                return true
            default:
                return false
            }
        }
        return false
    }
}
