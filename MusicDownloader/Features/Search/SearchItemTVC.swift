//
//  SearchItemTVC.swift
//  MusicDownloader
//
//  Created by son nguyen on 9/27/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class SearchItemTVC: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblArtist: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
