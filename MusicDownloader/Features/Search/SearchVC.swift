//
//  SecondViewController.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/20/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import PaginatedTableView
import Alamofire
import ObjectMapper
import SwipeCellKit

class SearchVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    weak var delegate: SongClickDelegate?
    var querry = ""
    var appDelegate: AppDelegate!
    var searchString: String?
    var timer: Timer?
    var songList: [Song] = []
    var tableDelegate = SongTableDelegate()
    var tableDataSource = SongTableDataSouce()
    var offset: Int = 25
    var cellID: String = "SongTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        hideKeyboardWhenTappedAround()
        setupPlaceholder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        searchTextField.becomeFirstResponder()
    }
    
    private func setupTable() {
        tableDataSource.songList = songList
        tableDelegate.songList = songList
        tableDataSource.swipeActions = [.favorite, .addPlaylist]
        tableDelegate.willLoadMore = true
        tableDelegate.loadMoreCallback = { [weak self] in
            self?.getMoreSearch(keyword: self?.querry ?? "", completion: { songs in
                if songs.count > 0 {
                    self?.songList.append(contentsOf: songs)
                    self?.tableDataSource.songList = self?.songList ?? []
                    self?.tableDelegate.songList = self?.songList ?? []
                    self?.tableView.reloadData()
                    
                } else {
                    self?.tableDelegate.willLoadMore = false
                }
            })
        }
        tableDelegate.didSelectCellCallback = { song, songList in
            didSelectCell(song: song, songList: songList, parentVC: self)
        }
        tableView.dataSource = tableDataSource
        tableView.delegate = tableDelegate
        tableView.register(UINib(nibName: cellID, bundle: nil), forCellReuseIdentifier: cellID)
    }
    
    private func setupPlaceholder() {
        searchTextField.attributedPlaceholder = NSAttributedString(string: "Search for your song, artist, albums", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor(hexString: "#BDBDBD")])
    }
    
    private func getMoreSearch(keyword: String, completion: @escaping (([Song])-> Void)){
        APIClient.shared.getSearch(keyword: keyword, offset: offset, completion: {[weak self] response in self?.offset += response.headers.results_count
            completion(response.results)
            }, errorHandler: nil
        )
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        guard let text = sender.text, text.count > 2 else {
            return
        }
        
        querry = text
        stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(getSongs(timer:)), userInfo: text, repeats: false)
    }
    
    @IBAction func hideKeyBoard(_ sender: Any) {
        searchTextField.endEditing(true)
    }
    
    private func stopTimer(){
        timer?.invalidate()
        timer = nil
    }
    
    @objc func getSongs(timer: Timer) {
        guard let text = timer.userInfo as? String else {
            return
        }

        APIClient.shared.getSearch(keyword: text, offset: 0, completion: { [weak self] response in
            self?.offset += response.headers.results_count
            self?.songList = response.results
            self?.tableDataSource.songList = response.results
            self?.tableDelegate.songList = response.results
            self?.tableView.reloadData()
        }, errorHandler: nil)
    }
}
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        guard list.count > 0 else { return }
//        delegate?.didSongSelect(song: list[indexPath.row])
//    }
    

extension SearchVC: SongClickDelegate {
    func didSongSelect(song: Song, songList: [Song]) {
        guard let playerView = storyboard?.instantiateViewController(withIdentifier: "PlayerViewViewController") as? PlayerViewViewController else { return }
        
        AVPlayerSingleton.shared.songList = songList
        _ = AVPlayerSingleton.shared.initPlayer(withSong: song)
        AVPlayerSingleton.shared.player?.play()
        playerView.modalPresentationStyle = .overCurrentContext
        appDelegate.setCurrentSong(song: song)
        DataManager.shared.isPlaying = true
        NotificationCenter.default.post(name: NSNotification.Name("removeMiniPlayerView"), object: nil)
        self.present(playerView, animated: true, completion: nil)
    }
}
