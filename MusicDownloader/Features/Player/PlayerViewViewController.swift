//
//  PlayerViewViewController.swift
//  MusicDownloader
//
//  Created by son nguyen on 9/3/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import CoreMedia

class PlayerViewViewController: UIViewController {
    
    // MARK: - Outlets:
    
    @IBOutlet weak var hideView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var lblSongName: UILabel!
    @IBOutlet weak var imgFavorite: UIImageView!
    @IBOutlet weak var imgAdd: UIImageView!
    @IBOutlet weak var bgPlayView: UIView!
    @IBOutlet weak var imgPlayStatus: UIImageView!
    @IBOutlet weak var vPrev: UIView!
    @IBOutlet weak var vNext: UIView!
    @IBOutlet weak var vDownload: UIView!
    @IBOutlet weak var vMore: UIView!
    @IBOutlet weak var lblCurrentDuration: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var sliderDuration: UISlider!
    @IBOutlet weak var artistLabel: GradientLabel!
    // MARK: - Properties:
    
    var appDelegate: AppDelegate!
    var songList: [Song] = []
    var dataManager = DataManager.shared
    let kNotiUpdateProgress = "update_progress"
    var addPlaylistPopover = AddPlayListPopover()
    var playFeaturePopover: PlayFeaturePopover!
    // MARK: - Life cycle:
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        setupUI()
        setupTapGesture()
        setupNotification()
        setupGradientView()
        gradientPlayView()
        setupSlider()
        updateFavoriteImage(for: AVPlayerSingleton.shared.song)
        saveToHistory()
        setupFeaturePopover()
    }
    
    private func setupNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateProgress),
                                               name: NSNotification.Name(kNotiUpdateProgress),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.updateView),
                                               name: NSNotification.Name("updateUIForCurrentSong"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.updateAddButton),
                                               name: NSNotification.Name("didHideAddPlaylistPopup"),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupFeaturePopover() {
        playFeaturePopover = PlayFeaturePopover()
        playFeaturePopover.hideBlock = { [weak self] in
            self?.playFeaturePopover.hide()
        }
        playFeaturePopover.showPopoverBlock = { [weak self] in
            let timerPopover = TimerViewController.loadFromNib()
            timerPopover.didSelectCallback = { hour, text in
                self?.playFeaturePopover.timerLength = hour
                self?.playFeaturePopover.timeLabel.text = text
            }
            
            timerPopover.modalPresentationStyle = .popover
            if let popoverVC = timerPopover.popoverPresentationController {
                popoverVC.sourceView = self?.playFeaturePopover.timeView
                popoverVC.sourceRect = self?.playFeaturePopover.timeView.bounds ?? CGRect.zero
                popoverVC.permittedArrowDirections = [.up]
                popoverVC.backgroundColor = .white
                popoverVC.delegate = self
            }
            self?.present(timerPopover, animated: true, completion: nil)
        }
    }
    
    private func saveToHistory() {
        guard let currentSong = AVPlayerSingleton.shared.song else { return }
        RealmManager.shared.saveToHistory(song: currentSong)
    }
    
    private func setupSlider() {
        let image = UIImage(named: "ic_oval")
        sliderDuration.setThumbImage(image, for: .normal)
        sliderDuration.tintColor = UIColor("#FDA2FF")
        sliderDuration.addTarget(self, action: #selector(numberValueChanged), for: UIControl.Event.valueChanged)
        guard let song = AVPlayerSingleton.shared.song else {
            sliderDuration.value = 0.0
            return
        }
        let value = Float(Int(AVPlayerSingleton.shared.timePlayed) * 100 / song.duration)
        sliderDuration.setValue(value, animated: false)
    }
    
    private func setupUI() {
        view.isOpaque = false
        view.backgroundColor = .clear
        
        guard let song = AVPlayerSingleton.shared.song else {
            lblCurrentDuration.text = Utils().getDurationText(duration: 0)
            return
        }
        
        lblCurrentDuration.text = Utils().getDurationText(duration: Int(AVPlayerSingleton.shared.timePlayed))
        let placeholderImage = UIImage(named: "ic_blur")!
        imgCover?.sd_setImage(with: URL(string: song.image), placeholderImage: placeholderImage)
        artistLabel.text = song.artist_name
        lblSongName.text = song.name
        lblDuration.text = Utils().getDurationText(duration: song.duration)
        imgPlayStatus.image = DataManager.shared.isPlaying ? UIImage(named: "ic_pause")! : UIImage(named: "ic_play")!
        
        setupFavoriteImage(for: song)
    }
    
    private func setupFavoriteImage(for song: Song) {
        let selectedSong: Song
        if let localSong = RealmManager.shared.getSongByID(song.id) {
            selectedSong = localSong
        } else {
            selectedSong = song
        }
        
        imgFavorite.image = UIImage(named: selectedSong.isFavorited ? "ic_heart" : "ic_heart_inactive")
    }
    
    private func setupTapGesture() {
        let tapStatusView = UITapGestureRecognizer(target: self, action: #selector(self.togglePlayer(_:)))
        bgPlayView.addGestureRecognizer(tapStatusView)
        
        let hideViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleDismiss(_:)))
        hideView.addGestureRecognizer(hideViewTapGesture)
        
        let morePopoverTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleDismissMorePopover))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(morePopoverTapGesture)
        
        let favortireTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleFavoriteDidTap))
        imgFavorite.addGestureRecognizer(favortireTapGesture)
        
        let addTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleAddDidTap))
        imgAdd.addGestureRecognizer(addTapGesture)
        
        let downLoadTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleDownloadDidTap))
        vDownload.addGestureRecognizer(downLoadTapGesture)
        
        let prevTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handlePrevDidTap))
        vPrev.addGestureRecognizer(prevTapGesture)
        
        let nextTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleNextDidTap))
        vNext.addGestureRecognizer(nextTapGesture)
        
        let moreTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleMoreDidTap))
        vMore.addGestureRecognizer(moreTapGesture)
    }
    
    @objc func handleDismissMorePopover() {
        if !dataManager.isShowingPlayFeature { return }
        playFeaturePopover.hide()
        dataManager.isShowingPlayFeature = !dataManager.isShowingPlayFeature
    }
    
    @objc func handleDownloadDidTap() {
//        guard !dataManager.isShowingAddPlaylist && !dataManager.isShowingPlayFeature else { return }
//        
//        guard let song = AVPlayerSingleton.shared.song else {
//            showNotification(notiType: .noSong)
//            return
//        }
//        APIClient.shared.download(song: song) { url in
//            RealmManager.shared.saveAfterDownload(song: song, url: url)
//        }
    }
    
    @objc func handlePrevDidTap() {
        guard !dataManager.isShowingAddPlaylist && !dataManager.isShowingPlayFeature else { return }
        if AVPlayerSingleton.shared.playPrevious() {
            self.updateView()
            saveToHistory()
        }
    }
    
    @objc func handleNextDidTap() {
        guard !dataManager.isShowingAddPlaylist && !dataManager.isShowingPlayFeature else { return }
        if AVPlayerSingleton.shared.playNext() {
            self.updateView()
            saveToHistory()
        }
    }
    
    @objc func handleMoreDidTap() {
//        if dataManager.isShowingAddPlaylist || dataManager.isShowingPlayFeature { return }
//        if !dataManager.isShowingPlayFeature {
//            playFeaturePopover.show(in: self.view)
//        }
//        dataManager.isShowingPlayFeature = !dataManager.isShowingPlayFeature
    }
    
    @objc func handleFavoriteDidTap() {
        guard !dataManager.isShowingAddPlaylist && !dataManager.isShowingPlayFeature else { return }
        guard let song = AVPlayerSingleton.shared.song else { return }
        RealmManager.shared.update(song: song, isFavorited: !song.isFavorited)
        updateFavoriteImage(for: song)
    }
    
    @objc func handleAddDidTap() {
        if dataManager.isShowingPlayFeature { return }
        addPlaylistPopover.currentSong = AVPlayerSingleton.shared.song
        if !dataManager.isShowingAddPlaylist {
            addPlaylistPopover.show()
        } else {
            addPlaylistPopover.hide()
        }
        dataManager.isShowingAddPlaylist = !dataManager.isShowingAddPlaylist
        updateAddButton()
    }
    
    @objc func updateView() {
        guard let song = AVPlayerSingleton.shared.song else { return }
        let value = Float(Int(AVPlayerSingleton.shared.timePlayed) * 100 / song.duration)
        sliderDuration.setValue(value, animated: true)
        let placeholderImage = UIImage(named: "ic_blur")!
        imgCover?.sd_setImage(with: URL(string: song.image), placeholderImage: placeholderImage)
        artistLabel.text = song.artist_name
        lblSongName.text = song.name
        lblCurrentDuration.text = Utils().getDurationText(duration: Int(AVPlayerSingleton.shared.timePlayed))
        lblDuration.text = Utils().getDurationText(duration: song.duration)
        imgPlayStatus.image = DataManager.shared.isPlaying ? UIImage(named: "ic_pause") : UIImage(named: "ic_play")
        setupFavoriteImage(for: song)
    }
    
    @objc func updateAddButton() {
        if dataManager.isShowingAddPlaylist {
            self.imgAdd.image = UIImage(named: "ic_close_popover")
        } else {
            self.imgAdd.image = UIImage(named: "ic_add")
        }
    }
    
    private func updateFavoriteImage(for song: Song?) {
        guard let song = song else {
            return
        }
        imgFavorite.image = UIImage(named: song.isFavorited ? "ic_heart" : "ic_heart_inactive")
    }
    
    @objc func handleDismiss(_ sender: UITapGestureRecognizer? = nil) {
        if !dataManager.isShowingAddPlaylist && !dataManager.isShowingPlayFeature {
            NotificationCenter.default.post(name: NSNotification.Name("showMiniPlayerView"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name("updateMiniPlayer"), object: nil)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func updateProgress() {
        if AVPlayerSingleton.shared.isEnd() {
            AVPlayerSingleton.shared.playNext()
            return
        }
        guard let song = AVPlayerSingleton.shared.song else { return }
        let value = Float(Int(AVPlayerSingleton.shared.timePlayed) * 100 / song.duration)
        sliderDuration.setValue(value, animated: true)
        lblCurrentDuration.text = Utils().getDurationText(duration: Int(AVPlayerSingleton.shared.timePlayed))
    }
    
    @objc func numberValueChanged(sender: UISlider) {
        getSeekToTime(value: sender.value)
    }
    
    func getSeekToTime(value: Float) {
        guard let song = AVPlayerSingleton.shared.song else { return }
        let time = round (Float(song.duration) * sliderDuration.value / 100)
        let myTime = CMTime(seconds: Double(time), preferredTimescale: 60000)
        AVPlayerSingleton.shared.player?.seek(to: myTime)
        lblCurrentDuration.text = Utils().getDurationText(duration: Int(time))
    }
    
    private func setupGradientView() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.gradientView.bounds
        gradientLayer.colors = [UIColor.black.withAlphaComponent(0.0).cgColor, UIColor.black.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        self.gradientView.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func gradientPlayView() {
        bgPlayView.roundCorners([.allCorners], radius: 28)
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bgPlayView.bounds
        gradientLayer.colors = [UIColor("#4D61FF").cgColor, UIColor("#FDA2FF").cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.bgPlayView.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    @objc func togglePlayer(_ sender: UITapGestureRecognizer? = nil) {
        guard AVPlayerSingleton.shared.song != nil else { return }
        
        guard !dataManager.isShowingAddPlaylist && !dataManager.isShowingPlayFeature else { return }
        
        if AVPlayerSingleton.shared.player?.rate == 1.0 {
            AVPlayerSingleton.shared.player?.pause()
            DataManager.shared.isPlaying = false
            imgPlayStatus.image =  UIImage(named: "ic_play")!
        } else {
            AVPlayerSingleton.shared.player?.play()
            DataManager.shared.isPlaying = true
            imgPlayStatus.image = UIImage(named: "ic_pause")!
        }
    }
}

extension PlayerViewViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
