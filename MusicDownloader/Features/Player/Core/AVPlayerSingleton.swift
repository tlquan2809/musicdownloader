//
//  AVPlayerSingleton.swift
//  MusicDownloader
//
//  Created by son nguyen on 9/7/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import Foundation
import MediaPlayer
import AVFoundation
import MediaPlayer

enum PlayType {
    case meditation
    case breathing
}

class AVPlayerSingleton {
    
    // MARK: - Properties
    
    // Singleton pattern
    static let shared = AVPlayerSingleton()
    private var playerTimeObserver: Any?
    
    // Timer
    var timePlayed: Double = 0
    
    // Player
    var player: AVPlayer?
    var isPlaying: Bool = false
    var playType: PlayType?
    let kNotiUpdateProgress = "update_progress"
    var audioPlayer: AVAudioPlayer?
    // Key-value observing context
    private var playerItemContext = 0
    // Get the shared MPRemoteCommandCenter
    private let commandCenter = MPRemoteCommandCenter.shared()
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    var song: Song?
    var songList: [Song]?
    var indexOfCurrentSong: Int = -1
    let nowPlayingCenter = MPNowPlayingInfoCenter.default()
    
    // MARK: - Functions
    
    func setupCommandCenter() {
        commandCenter.playCommand.isEnabled = true
        commandCenter.pauseCommand.isEnabled = true
        commandCenter.playCommand.addTarget { [weak self] (event) -> MPRemoteCommandHandlerStatus in
            self?.isPlaying = true
            self?.player?.play()
            DataManager.shared.isPlaying = true
            self?.nowPlayingCenter.nowPlayingInfo?.updateValue(1, forKey: MPNowPlayingInfoPropertyPlaybackRate)
            return .success
        }
        commandCenter.pauseCommand.addTarget { [weak self] (event) -> MPRemoteCommandHandlerStatus in
            self?.isPlaying = true
            self?.player?.pause()
            DataManager.shared.isPlaying = false
            self?.nowPlayingCenter.nowPlayingInfo?.updateValue(self?.timePlayed ?? 0, forKey: MPNowPlayingInfoPropertyElapsedPlaybackTime)
            self?.nowPlayingCenter.nowPlayingInfo?.updateValue(0, forKey: MPNowPlayingInfoPropertyPlaybackRate)
            return .success
        }
        commandCenter.nextTrackCommand.addTarget { [weak self] (event) -> MPRemoteCommandHandlerStatus in
            self?.playNext()
            return .success
        }
        commandCenter.previousTrackCommand.addTarget { [weak self] (event) -> MPRemoteCommandHandlerStatus in
            self?.playPrevious()
            return .success
        }
    }
    
    private func updateNowPlayingInfo(for song: Song) {
        nowPlayingCenter.nowPlayingInfo = [
            MPMediaItemPropertyTitle: song.name,
            MPMediaItemPropertyArtist: song.artist_name,
            MPMediaItemPropertyAlbumTitle: song.album_name,
            MPMediaItemPropertyPlaybackDuration: song.duration
        ]
        
        if let url = URL(string: song.album_image), let data = try? Data.init(contentsOf: url), let image = UIImage(data: data) {
            let artwork = MPMediaItemArtwork(boundsSize: image.size, requestHandler: { _ in
                return image
            })
            
            nowPlayingCenter.nowPlayingInfo?.updateValue(artwork, forKey: MPMediaItemPropertyArtwork)
        }
    }
    
    func initPlayer(withSong song: Song) -> AVPlayer {
        self.removeCurrentPlayer()
        self.timePlayed = 0
        self.song = song
        
        if let localSong = RealmManager.shared.getSongByID(song.id), !localSong.localUrl.isEmpty {
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent("\(song.id).mp3")
            self.player = AVPlayer(url: URL.init(fileURLWithPath: documentsURL.path))
        } else {
            let playerItem = AVPlayerItem.init(url: URL.init(string: song.audio)!)
            playerItem.preferredPeakBitRate = 2
            self.player = AVPlayer.init(playerItem: playerItem)
        }
        
        // Initialize self properties
        guard let player = self.player else {
            return AVPlayer(playerItem: nil)
        }
        
        // Add started playing observer
        let interval = CMTime.init(value: 1, timescale: 1)
        playerTimeObserver = player.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) { [weak self] time in
            guard let self = self else { return }
            self.isPlaying = true
            self.timePlayed = time.seconds
            NotificationCenter.default.post(name: NSNotification.Name(self.kNotiUpdateProgress), object: nil)
        }
        
        updateNowPlayingInfo(for: song)
        getIndex(of: song)
        return player
    }
    
    func seek(toSecond: Double) {
        self.player?.seek(to: CMTime(seconds: toSecond, preferredTimescale: CMTimeScale(NSEC_PER_SEC)))
    }
    
    func isEnd() -> Bool {
        return round(self.timePlayed) >= Double(self.song!.duration)
    }
    
    @objc private func play() {
        guard let player = player, !isPlaying else { return }
        isPlaying = true
        player.play()
    }
    
    @objc private func pause() {
        guard let player = player, isPlaying else { return }
        isPlaying = false
        player.pause()
    }
    
    func removeCurrentPlayer() {
        if let pto = playerTimeObserver {
            player?.removeTimeObserver(pto)
            playerTimeObserver = nil
        }
        
        self.isPlaying = false
        self.player = nil
    }
    
    func playNext() -> Bool {
        guard let currentSongList = self.songList else { return false }
        let nextIndex: Int
        // Loop playlist
        if DataManager.shared.isEnableShuffle {
            var randomIndex = -1
            repeat {
                let random = Int.random(in: 0 ..< currentSongList.count)
                randomIndex = random
            } while randomIndex == indexOfCurrentSong
            nextIndex = randomIndex
        } else {
            if indexOfCurrentSong < 0 {
                nextIndex = currentSongList.count - 1
            } else if indexOfCurrentSong >= currentSongList.count - 1 {
                nextIndex = 0
            } else {
                nextIndex = indexOfCurrentSong + 1
            }
        }
        
        let nextSong = currentSongList[nextIndex]
        DataManager.shared.isPlaying = true
        appDelegate!.setCurrentSong(song: nextSong)
        _ = self.initPlayer(withSong: nextSong)
        NotificationCenter.default.post(name: NSNotification.Name("updateUIForCurrentSong"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("updateMiniUIForCurrentSong"), object: nil)
        self.player?.play()
        return true
    }
    
    func playPrevious() -> Bool {
        guard let currentSongList = self.songList else { return false }
        let prevIndex: Int
        // Loop playlist
        if DataManager.shared.isEnableShuffle {
            var randomIndex = -1
            repeat {
                let random = Int.random(in: 0 ..< currentSongList.count)
                randomIndex = random
            } while randomIndex == indexOfCurrentSong
            prevIndex = randomIndex
        } else {
            if indexOfCurrentSong <= 0 {
                prevIndex = currentSongList.count - 1
            } else {
                prevIndex = indexOfCurrentSong - 1
            }
        }
        
        let prevSong = currentSongList[prevIndex]
        DataManager.shared.isPlaying = true
        appDelegate!.setCurrentSong(song: prevSong)
        _ = self.initPlayer(withSong: prevSong)
        NotificationCenter.default.post(name: NSNotification.Name("updateUIForCurrentSong"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("updateMiniUIForCurrentSong"), object: nil)
        self.player?.play()
        return true
    }
    
    private func getIndex(of song: Song) {
        guard let songList = self.songList else { return }
        for index in 0..<songList.count {
            if song.id == songList[index].id {
                indexOfCurrentSong = index
            }
        }
    }
}
