//
//  AddPlayListPopover.swift
//  MusicDownloader
//
//  Created by Dung Nguyen on 9/30/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class AddPlayListPopover: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var playlists: [Playlist] = []
    var currentSong: Song?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("AddPlayListPopover", owner: self, options: nil)
        addSubview(contentView)
        contentView.layer.cornerRadius = 8
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.register(UINib.init(nibName: "PlaylistPopoverCell", bundle: nil), forCellReuseIdentifier: "PlaylistPopoverCell")
    }
    
//    func show(in view: UIView) {
//        view.addSubview(self)
//        view.bringSubviewToFront(self)
//        contentView.frame = CGRect(x: UIScreen.main.bounds.width / 2 - 150, y: UIScreen.main.bounds.height, width: 300, height: 220)
//        contentView.alpha = 0
//        UIView.animate(withDuration: 0.4) {
//            self.contentView.alpha = 1
//            self.contentView.center = CGPoint(x: view.center.x, y: view.center.y - 60)
//        }
//    }
    
     func show() {
        guard let rootViewController = UIApplication.shared.keyWindow?.rootViewController as? HomeTabbarController else {
            return
        }
        
        NotificationCenter.default.post(name: NSNotification.Name("didShowAddPlaylistPopup"), object: nil)
        playlists = RealmManager.shared.getAllPlaylist().compactMap({ $0 })
        tableView.reloadData()
        rootViewController.view.addSubview(self)
        rootViewController.view.bringSubviewToFront(self)
        self.frame = CGRect(x: UIScreen.main.bounds.width / 2 - 167.5, y: UIScreen.main.bounds.height, width: 335, height: 220)
        contentView.alpha = 0
        UIView.animate(withDuration: 0.3) {
            rootViewController.showTransparentView()
            self.contentView.alpha = 1
            self.center = CGPoint(x: rootViewController.view.center.x, y: rootViewController.view.center.y - 60)
        }
    }
    
    func hide() {
        guard let rootViewController = UIApplication.shared.keyWindow?.rootViewController as? HomeTabbarController else {
            return
        }
        
        NotificationCenter.default.post(name: NSNotification.Name("didHideAddPlaylistPopup"), object: nil)
        UIView.animate(withDuration: 0.15, animations: {
            rootViewController.hideTranparentView()
            self.contentView.alpha = 1
            self.center.y = UIScreen.main.bounds.height
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }
    
    @IBAction func createButtonDidTap(_ sender: UIButton) {
        guard let superView = self.superview, !DataManager.shared.isShowingCreateNewPlaylist else {
            return
        }
        let view = CreateNewPlaylistPopover()
        view.delegate = self
        self.hide()
        DataManager.shared.isShowingAddPlaylistAgain = true
        view.currentSong = self.currentSong
        view.show(in: superView)
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        DataManager.shared.isShowingAddPlaylist = false
        hide()
    }
}

extension AddPlayListPopover: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlaylistPopoverCell") as? PlaylistPopoverCell else {
            return UITableViewCell()
        }
        
        guard let song = currentSong else {
            return UITableViewCell()
        }
        
        let pl = playlists[indexPath.row]
        cell.configCell(name: pl.name)
        cell.didAddCallback = { [weak self] in
            _ = RealmManager.shared.save(song: song)
            RealmManager.shared.update(playlist: pl, with: song)
            NotificationCenter.default.post(name: NSNotification.Name("didHideAddPlaylistPopup"), object: nil)
            DataManager.shared.isShowingAddPlaylist = false
            self?.hide()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        44
    }
}

extension AddPlayListPopover: PlaylistDelegate {
    func reloadData() {
        playlists = RealmManager.shared.getAllPlaylist().compactMap({ $0 })
        tableView.reloadData()
    }
}
