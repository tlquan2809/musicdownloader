//
//  TimerViewController.swift
//  MusicDownloader
//
//  Created by Quan Tran on 19/01/2021.
//  Copyright © 2021 son nguyen. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController {
    
    var didSelectCallback: ((Int, String?) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        preferredContentSize = CGSize(width: 106, height: 140)
    }
    
    @IBAction func selectLength(_ sender: UIButton) {
        let tag = sender.tag
        let text = sender.titleLabel?.text
        dismiss(animated: true, completion: { [weak self] in
            self?.didSelectCallback?(tag, text)
        })
    }
}
