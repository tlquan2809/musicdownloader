//
//  PlayFeaturePopover.swift
//  MusicDownloader
//
//  Created by Dung Nguyen on 9/30/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class PlayFeaturePopover: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var shareImageView: UIImageView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var shuffleSwitch: UISwitch!
    
    var dataManager = DataManager.shared
    var timerLength: Int = 2
    var showPopoverBlock: (() -> ())?
    var hideBlock: (() -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("PlayFeaturePopover", owner: self, options: nil)
        addSubview(contentView)
        contentView.layer.cornerRadius = 8
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        timeView.layer.cornerRadius = timeView.frame.height / 2
        setupTapGesture()
        setupShuffleSwitch()
    }
    
    private func setupShuffleSwitch() {
        shuffleSwitch.isOn = dataManager.isEnableShuffle
    }
    
     private func setupTapGesture() {
        let shareTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleShareImageDidTap))
        shareImageView.addGestureRecognizer(shareTapGesture)
        
        let timeViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTimeViewDidTap))
        timeView.addGestureRecognizer(timeViewTapGesture)
    }
    
    @objc func handleShareImageDidTap() {
        print("handleShareImageDidTap")
    }
    
    @objc func handleTimeViewDidTap() {
        showPopoverBlock?()
    }
    
    func show(in view: UIView) {
        view.addSubview(self)
        view.bringSubviewToFront(self)
        self.frame = CGRect(x: UIScreen.main.bounds.width / 2 - 167.5, y: UIScreen.main.bounds.height, width: 365, height: 220)
        contentView.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.contentView.alpha = 1
            self.center = CGPoint(x: view.center.x, y: view.center.y + 100)
        }
    }
    
    func hide() {
        UIView.animate(withDuration: 0.15, animations: {
            self.contentView.alpha = 1
            self.center.y = UIScreen.main.bounds.height
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }
    
    @IBAction func shuffleSwitchDidTap(_ sender: UISwitch) {
        dataManager.isEnableShuffle = !dataManager.isEnableShuffle
        shuffleSwitch.setOn(dataManager.isEnableShuffle, animated: true)
    }
    
    @IBAction func startTimer(_ sender: UIButton) {
        let notificationCenter = UNUserNotificationCenter.current()
        let id = "pauseTimer"
        notificationCenter.removePendingNotificationRequests(withIdentifiers: [id])
        
        let content = UNMutableNotificationContent()
        content.title = ""
        content.body = ""
        let aps: [String: Any] = ["content-available": 1]
        let userInfo: [String: Any] = ["aps": aps]
        content.userInfo = userInfo
        
        let date = Date()
        let dateTrigger = date.addingTimeInterval(TimeInterval(timerLength * 60))
        var dateComponents = DateComponents()
        dateComponents.calendar = Calendar.current
        dateComponents.hour = Calendar.current.component(.hour, from: dateTrigger) + timerLength
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
        notificationCenter.add(request) { (error) in
            if let theError = error {
                print(theError.localizedDescription)
            }
        }
        
        dataManager.isShowingPlayFeature = !dataManager.isShowingPlayFeature
        hide()
    }
}

