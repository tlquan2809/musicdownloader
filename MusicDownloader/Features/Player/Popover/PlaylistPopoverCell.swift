//
//  PlaylistPopoverCell.swift
//  MusicDownloader
//
//  Created by Dung Nguyen on 9/30/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class PlaylistPopoverCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addImageView: UIImageView!
    
    var didAddCallback: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapGesture = UITapGestureRecognizer( target: self, action: #selector(handleAddDidTap))
        tapGesture.delegate = self
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tapGesture)
    }
    
    func configCell(name: String) {
        nameLabel.text = name
    }
    
    @objc func handleAddDidTap() {
        didAddCallback?()
    }
}
