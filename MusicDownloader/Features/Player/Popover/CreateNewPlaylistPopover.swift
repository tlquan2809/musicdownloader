//
//  CreateNewPlaylistPopover.swift
//  MusicDownloader
//
//  Created by Tung Nguyen on 10/4/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import RealmSwift

class CreateNewPlaylistPopover: UIView {
    
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var namePlaylistTextField: UITextField!
    @IBOutlet weak var closeButton: UIButton!
    weak var delegate: PlaylistDelegate?
    var currentSong: Song?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    private func setupUI() {
        Bundle.main.loadNibNamed("CreateNewPlaylistPopover", owner: self, options: nil)
        addSubview(contentView)
        contentView.layer.cornerRadius = 8
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        namePlaylistTextField.layer.cornerRadius = 8
        namePlaylistTextField.delegate = self
        namePlaylistTextField.becomeFirstResponder()
        namePlaylistTextField.textColor = UIColor.white
        namePlaylistTextField.backgroundColor = UIColor.black
        setupCreateButton()
        setupTapGesture()
    }
    
    private func setupTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleViewDidTap))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleViewDidTap() {
        self.hide()
    }
    
    private func setupCreateButton() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.createButton.bounds
        gradientLayer.colors = [UIColor("#4D61FF").cgColor, UIColor("#FDA2FF").cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        self.createButton.layer.insertSublayer(gradientLayer, at: 0)
        
        createButton.layer.cornerRadius = 14
    }
    
    func show(in view: UIView) {
        guard let rootViewController = UIApplication.shared.keyWindow?.rootViewController as? HomeTabbarController else {
            return
        }
        
        self.frame = CGRect(x: UIScreen.main.bounds.width / 2 - 167.5, y: UIScreen.main.bounds.height, width: 335, height: 220)
        rootViewController.view.addSubview(self)
        rootViewController.view.bringSubviewToFront(self)
        
        contentView.alpha = 0
        DataManager.shared.isShowingCreateNewPlaylist = true
        rootViewController.showTransparentView()
        UIView.animate(withDuration: 0.3) {
            self.contentView.alpha = 1
            self.center = CGPoint(x: rootViewController.view.center.x, y: rootViewController.view.center.y - 60)
        }
    }
    
    func hide() {
        guard let rootViewController = UIApplication.shared.keyWindow?.rootViewController as? HomeTabbarController else {
            return
        }

        rootViewController.hideTranparentView()
        UIView.animate(withDuration: 0.15, animations: {
            self.contentView.alpha = 1
            self.center.y = UIScreen.main.bounds.height
        }, completion: { _ in
            DataManager.shared.isShowingCreateNewPlaylist = false
            self.delegate?.reloadData()
            self.removeFromSuperview()
            if DataManager.shared.isShowingAddPlaylistAgain == true {
                let addToPlaylist = AddPlayListPopover()
                addToPlaylist.currentSong = self.currentSong
                addToPlaylist.show()
                DataManager.shared.isShowingAddPlaylistAgain = false
            }
        })
    }
    
    @IBAction func createButtonTapped(_ sender: Any) {
        guard let playlistName = namePlaylistTextField.text, !playlistName.isEmpty else {
            return
        }
        
        let newPlaylist = Playlist(name: playlistName, songs: List<String>())
        _ = RealmManager.shared.save(playlist: newPlaylist)
        self.hide()
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.hide()
    }
}

extension CreateNewPlaylistPopover: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
