//
//  PopupPlayerVC.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/31/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

protocol PlayerDelegate:NSObjectProtocol {
    func didTapOnPlayer()
    //    func scrollStatus(isTop:Bool)
    
}

class PopupPlayerVC: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    weak var delegate:PlayerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        topView.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        //        delegate?.didTapOnPlayer()
        let pl = self.storyboard?.instantiateViewController(withIdentifier: "PlayerViewViewController") as? PlayerViewViewController
        pl!.modalPresentationStyle = .overCurrentContext
        
        self.present(pl!, animated: true, completion: nil)
        //        self.navigationController?.pushViewController(pl!, animated: true)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
