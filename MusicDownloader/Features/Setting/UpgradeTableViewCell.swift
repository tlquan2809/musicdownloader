//
//  UpgradeTableViewCell.swift
//  MusicDownloader
//
//  Created by Tung Nguyen on 10/5/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class UpgradeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var upgradeImageView: UIImageView!
    @IBOutlet weak var upgradeButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var buttonImageView1: UIImageView!
    @IBOutlet weak var buttonImageView2: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var upgradeImageTopConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupUI()
        self.setupConstraint()
    }
    
    private func setupUI() {
        self.backgroundColor = UIColor.black
        
        self.backgroundImageView.image = UIImage(named: "bg_setting")
        
        self.upgradeImageView.image = UIImage(named: "bg_setting_mem")
        self.upgradeImageView.layer.cornerRadius = 10
        
        self.upgradeButton.setBackgroundImage(UIImage(named: "bg_setting_upgrade_button"), for: .normal)
        
        
        let upgradeAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 15),
            .foregroundColor: UIColor.black
        ]
        let upgradeButtonTitle = NSAttributedString(string: "UPGRADE NOW", attributes: upgradeAttributes)
        self.upgradeButton.setAttributedTitle(upgradeButtonTitle, for: .normal)
        self.upgradeButton.layer.cornerRadius = 10
        
        let messageAtrributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 13),
            .foregroundColor: UIColor.yellow ]
        self.messageLabel.attributedText = NSAttributedString(string: "Remove ads forever!\nUpgrade to premium member without ads!", attributes: messageAtrributes)
        
        self.buttonImageView1.image = UIImage(named: "ic_crown")
        self.buttonImageView2.image = UIImage(named: "ic_crown")
    }
    
    private func setupConstraint() {
        if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
            case 1136, 1334 ,1920, 2208:
                self.upgradeImageTopConstraint.constant = 40
            case 2436, 2688, 1792:
                self.upgradeImageTopConstraint.constant = 60
            default:
                break
            }
        }
    }
    
    @IBAction func upgradeButtonTapped(_ sender: Any) {
        print("upgrade")
    }
}
