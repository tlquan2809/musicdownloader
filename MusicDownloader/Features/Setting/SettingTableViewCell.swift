//
//  SettingTableViewCell.swift
//  MusicDownloader
//
//  Created by Tung Nguyen on 9/30/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {
    @IBOutlet weak var settingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.setupUI()
    }
    
    func setupUI() {
        self.contentView.backgroundColor = UIColor.black
        self.settingLabel.textColor = UIColor.white
    }
    
    func config(setting: String) {
        self.settingLabel.text = setting
    }
}
