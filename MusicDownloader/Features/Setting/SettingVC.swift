//
//  SettingVC.swift
//  MusicDownloader
//
//  Created by Tung Nguyen on 9/29/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class SettingVC: BaseViewController {
    
    // MARK: -- Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: -- Properties
//    let settings = ["Rating App", "Share with Friend", "More Apps", "Privacy & Terms", "Restore Purchase"]

    let settings = ["Rating App", "Share with Friend", "More Apps", "Privacy & Terms"]
    let settingTableViewCell = "SettingTableViewCell"
    let kSettingTableViewCell = "kSettingTableViewCell"
    let upgradeTableViewCell = "UpgradeTableViewCell"
    let kUpgradeTableViewCell = "kUpgradeTableViewCell"
    
    // MARK: -- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        self.setupTableView()
    }
    
    // MARK: -- Functions
    
    func setupUI() {
        self.view.backgroundColor = UIColor.black
        self.tableView.backgroundColor = UIColor.black
    }
    
    func setupTableView() {
        self.tableView.contentInsetAdjustmentBehavior = .never
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(UINib(nibName: self.settingTableViewCell, bundle: nil), forCellReuseIdentifier: self.kSettingTableViewCell)
        self.tableView.register(UINib(nibName: self.upgradeTableViewCell, bundle: nil), forCellReuseIdentifier: self.kUpgradeTableViewCell)
    }
}

// MARK: -- TableView DataSource

extension SettingVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count //+ 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row == 0 {
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: self.kUpgradeTableViewCell, for: indexPath) as? UpgradeTableViewCell else {
//                return UITableViewCell()
//            }
//
//            return cell
//        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: self.kSettingTableViewCell, for: indexPath) as? SettingTableViewCell else {
                return UITableViewCell()
            }
            
            cell.config(setting: settings[indexPath.row])
            
            return cell
//        }
    }
}

// MARK: -- TableView Delegate
extension SettingVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("------------\(indexPath.row)")
        switch indexPath.row {
        
        case 0:
            
            guard let url = URL(string: "itms-apps://itunes.apple.com/app/id1502349347") else {
                return
            }
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)

            } else {
                UIApplication.shared.openURL(url)
            }
            break
            
        case 1:
            
            let textToShare = "Check out this app!"
            if let myWebsite = NSURL(string: "https://apps.apple.com/us/app/id1502349347") {
                let objectsToShare = [textToShare, myWebsite] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                if let popOver = activityVC.popoverPresentationController {
                    popOver.sourceView = self.tableView
                }
                self.present(activityVC, animated: true, completion: nil)
            }
            
            break
            
        case 2:
            
            guard let url = URL(string: "https://apps.apple.com/us/developer/thao-nguyen/id930075717") else { return }
            UIApplication.shared.open(url)
            
            break
            
        case 3:
            
            guard let url = URL(string: "https://bachnx109.github.io/boxloca") else { return }
            UIApplication.shared.open(url)
            
            break
        default: break
            
        }
        
    }
}
