//
//  FirstViewController.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/20/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import Alamofire

class HomeVC: BaseViewController {
    
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchView: DesignableView!
    
    var listPopular: [Song] = []
    var listListened: [Song] = []
    var listDownload: [Song] = []
    
    var listPop: [Song] = []
    var listRock: [Song] = []
    var listEdm: [Song] = []
    var listChillout: [Song] = []
    var listClassic: [Song] = []
    
    let popularTableViewCellID = "PopularTableViewCell"
    let topListenedTableViewCellID = "TopListenedTableViewCell"
    let topDownloadTableViewCellID = "TopDownloadTableViewCell"
    let topGenresTableViewCellID = "TopGenresTableViewCell"
    
    var homeItem = ["Popular", "Top Listened", "Top Download", "Top Genres"]
    var countComplete = 0
    var appDelegate: AppDelegate!
    
    var didLoadAllGenres: [Bool] = [false, false, false, false, false]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        
        setupUI()
    }
    
    private func setupUI() {
        setupTableView()
        setupSections()
        getGenres()
        setupGesture()
    }
    
    private func setupSections() {
        getPopular()
        getListened()
        getDownload()
    }
    
    private func getGenres() {
        getGenres(.pop)
        getGenres(.rock)
        getGenres(.electronic)
        getGenres(.chillout)
        getGenres(.classical)
    }
    
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        let nib = UINib(nibName: popularTableViewCellID, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: popularTableViewCellID)
        
        let topListenedNib = UINib(nibName: topListenedTableViewCellID, bundle: nil)
        tableView.register(topListenedNib, forCellReuseIdentifier: topListenedTableViewCellID)
        
        let topDownloadNib = UINib(nibName: topDownloadTableViewCellID, bundle: nil)
        tableView.register(topDownloadNib, forCellReuseIdentifier: topDownloadTableViewCellID)
        
        let topGenresNib = UINib(nibName: topGenresTableViewCellID, bundle: nil)
        tableView.register(topGenresNib, forCellReuseIdentifier: topGenresTableViewCellID)
    }
    
    private func setupGesture() {
        let searchTap = UITapGestureRecognizer(target: self, action: #selector(switchToSearch))
        searchView.addGestureRecognizer(searchTap)
    }
    
    @objc private func switchToSearch() {
        tabBarController?.selectedIndex = 1
    }
    
    private func reloadTable() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension HomeVC {
    func getPopular() {
        APIClient.shared.getPopular(completion: { [weak self]response in
            self?.listPopular = response.results
            self?.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            }, errorHandler: nil)
    }
    
    
    func getListened() {
        APIClient.shared.getListened(offset: 0, completion: { [weak self]response in
            self?.listListened = response.results
            self?.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
            }, errorHandler: nil)
    }
    
    func getDownload() {
        APIClient.shared.getDownloads(offset: 0, completion: { [weak self]response in
            self?.listDownload = response.results
            self?.tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
            }, errorHandler: nil)
    }
    
    func getGenres(_ genre: Genres) {
        APIClient.shared.getGenres(genre, offset: 0, completion: { [weak self]response in
            switch genre {
            case .pop:
                self?.listPop = response.results
                self?.didLoadAllGenres[0] = true
            case .rock:
                self?.listRock = response.results
                self?.didLoadAllGenres[1] = true
            case .electronic:
                self?.listEdm = response.results
                self?.didLoadAllGenres[2] = true
            case .chillout:
                self?.listChillout = response.results
                self?.didLoadAllGenres[3] = true
            case .classical:
                self?.listClassic = response.results
                self?.didLoadAllGenres[4] = true
            }
            
            self?.checkGenresDownload()
            }, errorHandler: nil)
    }
    
    private func checkGenresDownload() {
        guard didLoadAllGenres.first(where: { !$0 }) == nil else {
            return
        }
        
        tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .automatic)
    }
}
extension HomeVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
            
        case 0 :
            let cell = tableView.dequeueReusableCell(withIdentifier: popularTableViewCellID, for: indexPath) as! PopularTableViewCell
            if cell.list != listPopular {
                cell.list = listPopular
                cell.collectionView.reloadData()
            }
            cell.delegate = self
            return cell
        case 1 :
            let cell = tableView.dequeueReusableCell(withIdentifier: topListenedTableViewCellID, for: indexPath) as! TopListenedTableViewCell
            if cell.list != listListened {
                cell.list = listListened
                cell.collectionView.reloadData()
            }
            cell.delegate = self
            cell.moreCallback = { [weak self] in
                if let listened = self?.listListened, listened.count > 0 {
                    let topListenedVC = TopListenedViewController.loadFromNib()
                    topListenedVC.songList = listened
                    self?.navigationController?.pushViewController(topListenedVC, animated: true)
                }
            }
            
            return cell
        case 2 :
            let cell = tableView.dequeueReusableCell(withIdentifier: topDownloadTableViewCellID, for: indexPath) as! TopDownloadTableViewCell
            if cell.list != listDownload {
                cell.list = listDownload
                cell.collectionView.reloadData()
            }
            cell.delegate = self
            cell.moreCallback = { [weak self] in
                if let download = self?.listDownload, download.count > 0 {
                    let topDownloadVC = UIViewController.instantiateFromStoryboard(storyboardIdentifier: "TopDownloadViewController", storyboardName: "TopDownloadViewController", bundle: nil) as! TopDownloadViewController
                    topDownloadVC.songList = download
                    self?.navigationController?.pushViewController(topDownloadVC, animated: true)
                }
            }
            
            return cell
        case 3 :
            let cell = tableView.dequeueReusableCell(withIdentifier: topGenresTableViewCellID, for: indexPath) as! TopGenresTableViewCell
            
            if cell.listPop != listPop || cell.listRock != listRock || cell.listClassic != listClassic || cell.listChillout != listChillout || cell.listEdm != listEdm {
                cell.listPop = listPop
                cell.listRock = listRock
                cell.listClassic = listClassic
                cell.listChillout = listChillout
                cell.listEdm = listEdm
                cell.collectionView.reloadData()
            }
            cell.moreCallback = { [weak self] in
                let topGenresVC = TopGenresViewController.loadFromNib()
                topGenresVC.listPop = self?.listPop ?? []
                topGenresVC.listRock = self?.listRock ?? []
                topGenresVC.listEdm = self?.listEdm ?? []
                topGenresVC.listChillout = self?.listChillout ?? []
                topGenresVC.listClassic = self?.listClassic ?? []
                
                self?.navigationController?.pushViewController(topGenresVC, animated: true)
            }
            cell.delegate = self
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 253
        case 1:
            return 350
        case 2:
            return 245
        case 3:
            return 500
        default:
            return 273
        }   
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 253
        case 1:
            return 350
        case 2:
            return 245
        case 3:
            return 500
        default:
            return 273
        }
    }
}

extension HomeVC: SongClickDelegate {
    func didSongSelect(song: Song, songList: [Song]) {
        guard let playerView = storyboard?.instantiateViewController(withIdentifier: "PlayerViewViewController") as? PlayerViewViewController else { return }
        AVPlayerSingleton.shared.songList = songList
        _ = AVPlayerSingleton.shared.initPlayer(withSong: song)
        AVPlayerSingleton.shared.player?.play()
        playerView.modalPresentationStyle = .overCurrentContext
        appDelegate.setCurrentSong(song: song)
        DataManager.shared.isPlaying = true
        NotificationCenter.default.post(name: NSNotification.Name("removeMiniPlayerView"), object: nil)
        self.present(playerView, animated: true, completion: nil)
    }
}
