//
//  TopListenedCollectionViewCell.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/23/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class TopListenedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgSong: UIImageView!
    
    @IBOutlet weak var lblSongName: UILabel!
    @IBOutlet weak var lblArtist: UILabel!
    
    @IBOutlet weak var lblDuration: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
