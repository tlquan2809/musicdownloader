//
//  TopListenedTableViewCell.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/22/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import Blueprints

class TopListenedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var moreView: UIView!
    
    var list: [Song] = []
    var moreCallback: (() -> Void)?
    var delegate: SongClickDelegate?
    let collectionCellID = "TopListenedCollectionViewCell"

    let blueprintLayout = HorizontalBlueprintLayout(
        itemsPerRow: 1.0,
        itemsPerColumn: 4,
        height: 60,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: EdgeInsets(top: 0, left: 16, bottom: 10, right: 10),
        stickyHeaders: false,
        stickyFooters: false
    )
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupCollectionView()
        setupGesture()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    private func setupCollectionView() {
        collectionView.collectionViewLayout = blueprintLayout
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: collectionCellID, bundle: nil), forCellWithReuseIdentifier: collectionCellID)
    }
    
    private func setupGesture() {
        let moreTap = UITapGestureRecognizer(target: self, action: #selector(moreAction))
        moreView.addGestureRecognizer(moreTap)
    }
    
    @objc private func moreAction() {
        moreCallback?()
    }
}

extension TopListenedTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard  let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellID, for: indexPath) as? TopListenedCollectionViewCell else{
                return UICollectionViewCell()
        }
        
        let data = list[indexPath.row]
        let placeholderImage = UIImage(named: "ic_blur")!
        
        cell.imgSong?.sd_setImage(with: URL(string:data.image),placeholderImage:placeholderImage)
        cell.lblSongName.text = data.name
        cell.lblArtist.text = data.artist_name
        cell.lblDuration.text = Utils().getDurationText(duration: data.duration)

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        AVPlayerSingleton.shared.songList = list
        delegate?.didSongSelect(song: list[indexPath.row], songList: list)
    }
}

