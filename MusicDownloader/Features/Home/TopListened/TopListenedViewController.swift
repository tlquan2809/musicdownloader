//
//  TopListenedViewController.swift
//  MusicDownloader
//
//  Created by Quan Tran on 10/11/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class TopListenedViewController: BaseViewController {

    @IBOutlet weak var searchView: DesignableView!
    @IBOutlet weak var songTable: UITableView!
    
    var songList: [Song] = []
    var tableDelegate = SongTableDelegate()
    var tableDataSource = SongTableDataSouce()
    var offset: Int = 25
    var cellID: String = "SongTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTable()
        setupGesture()
    }
    
    private func setupGesture() {
        let searchTap = UITapGestureRecognizer(target: self, action: #selector(switchToSearch))
        searchView.addGestureRecognizer(searchTap)
    }
    
    @objc private func switchToSearch() {
        tabBarController?.selectedIndex = 1
    }
    
    private func setupTable() {
        tableDataSource.songList = songList
        tableDelegate.songList = songList
        tableDataSource.swipeActions = [.favorite, .addPlaylist]
        tableDelegate.willLoadMore = true
        tableDelegate.loadMoreCallback = { [weak self] in
            self?.getListened()
        }
        tableDelegate.didSelectCellCallback = { song, songList in
            didSelectCell(song: song, songList: songList, parentVC: self)
        }
        songTable.delegate = tableDelegate
        songTable.dataSource = tableDataSource
        songTable.register(UINib(nibName: cellID, bundle: nil), forCellReuseIdentifier: cellID)
    }
    
    func getListened() {
        APIClient.shared.getListened(offset: offset, completion: { [weak self]response in
            if response.headers.results_count == 0 {
                return
            }
            
            self?.offset += response.headers.results_count
            self?.songList.append(contentsOf: response.results)
            
            if let songList = self?.songList {
                self?.tableDataSource.songList = songList
                self?.tableDelegate.songList = songList
                self?.songTable.reloadData()
            }
            }, errorHandler: nil)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
