//
//  TopDownloadTableViewCell.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/23/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class TopDownloadTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var moreView: UIView!
    @IBOutlet weak var titleforCell: UILabel!
    @IBOutlet weak var loadMoreLabel: UILabel!
    @IBOutlet weak var directMoreImage: UIImageView!
    
    var list: [Song] = []
    var delegate: SongClickDelegate?
    var moreCallback: (() -> Void)?
    let collectionCellID: String = "TopDownloadCollectionViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupCollectionView()
        setupGesture()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    private func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: collectionCellID, bundle: nil), forCellWithReuseIdentifier: collectionCellID)
    }
    
    private func setupGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openTopDownload))
        moreView.addGestureRecognizer(tapGesture)
    }
    
    @objc private func openTopDownload() {
        moreCallback?()
    }
}
extension TopDownloadTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellID, for: indexPath) as? TopDownloadCollectionViewCell else {
                return UICollectionViewCell()
        }
        
        let data = list[indexPath.row]
        
        let placeholderImage = UIImage(named: "ic_blur")!
        cell.imgSong?.sd_setImage(with: URL(string:data.image),placeholderImage:placeholderImage)
        cell.lblName.text = data.name
        cell.lblArtist.text = data.artist_name

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:  130, height: 185)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        AVPlayerSingleton.shared.songList = list
        delegate?.didSongSelect(song: list[indexPath.row], songList: list)
    }
    
}

