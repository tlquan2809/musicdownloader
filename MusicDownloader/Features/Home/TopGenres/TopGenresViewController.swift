//
//  TopGenresViewController.swift
//  MusicDownloader
//
//  Created by Quan Tran on 10/1/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class TopGenresViewController: BaseButtonBarPagerTabStripViewController<GenreCollectionViewCell> {
    
    @IBOutlet weak var searchView: DesignableView!
    
    let genreList: [Genres] = [.classical, .pop, .rock, .electronic, .chillout]
    var listPop: [Song] = []
    var listRock: [Song] = []
    var listEdm: [Song] = []
    var listChillout: [Song] = []
    var listClassic: [Song] = []
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        setupPager()
        super.viewDidLoad()
        
        setupGesture()
    }
    
    private func setupPager() {
        buttonBarItemSpec = ButtonBarItemSpec.nibFile(nibName: "GenreCollectionViewCell", bundle: nil, width: { _ in
            return 40
        })
        
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = .clear
        settings.style.selectedBarHeight = 0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        
        changeCurrentIndexProgressive = { (oldCell: GenreCollectionViewCell?, newCell: GenreCollectionViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.genreImage.alpha = 0.5
            oldCell?.imageHeightConstraint.constant = 30
            
            newCell?.genreImage.alpha = 1
            newCell?.imageHeightConstraint.constant = 40
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let vc1 = GenresViewController.loadFromNib()
        vc1.genre = .classical
        vc1.songList = listClassic
        
        let vc2 = GenresViewController.loadFromNib()
        vc2.genre = .pop
        vc2.songList = listPop
        
        let vc3 = GenresViewController.loadFromNib()
        vc3.genre = .rock
        vc3.songList = listRock
        
        let vc4 = GenresViewController.loadFromNib()
        vc4.genre = .electronic
        vc4.songList = listEdm
        
        let vc5 = GenresViewController.loadFromNib()
        vc5.genre = .chillout
        vc5.songList = listChillout
        
        return [vc1, vc2, vc3, vc4, vc5]
    }
    
    override func configure(cell: GenreCollectionViewCell, for indicatorInfo: IndicatorInfo) {
        cell.genreImage.image = indicatorInfo.image
    }
    
    func getGenres(_ genre: Genres, completion: (([Song]) -> Void)?) {
        APIClient.shared.getGenres(genre, offset: 25, completion: { response in
            completion?(response.results)
            }, errorHandler: nil)
    }
    
    private func setupGesture() {
        let searchTap = UITapGestureRecognizer(target: self, action: #selector(switchToSearch))
        searchView.addGestureRecognizer(searchTap)
    }
    
    @objc private func switchToSearch() {
        tabBarController?.selectedIndex = 1
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
