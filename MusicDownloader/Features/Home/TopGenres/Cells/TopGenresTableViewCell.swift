//
//  TopGenresTableViewCell.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/24/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class TopGenresTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var moreView: UIView!
    
    var listPop: [Song] = []
    var listRock: [Song] = []
    var listEdm: [Song] = []
    var listChillout: [Song] = []
    var listClassic: [Song] = []
    
    let collectionViewCellID: String = "TopGenresCollectionViewCell"
    var delegate: SongClickDelegate?
    var moreCallback: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupCollectionView()
        setupGesture()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    private func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: collectionViewCellID, bundle: nil), forCellWithReuseIdentifier: collectionViewCellID)
    }
    
    private func setupGesture() {
        let tapMoreGesture = UITapGestureRecognizer(target: self, action: #selector(moreAction))
        moreView.addGestureRecognizer(tapMoreGesture)
    }
    
    @objc private func moreAction() {
        moreCallback?()
    }
    
    @IBAction func tap(_ sender: HomeTopGenresTapGestureRecognizer) {
        guard let tapedSong = sender.tapedSong, let songList = sender.songList else {
            return
        }
        
        AVPlayerSingleton.shared.songList = songList
        delegate?.didSongSelect(song: tapedSong, songList: songList)
    }
}
extension TopGenresTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewCellID, for: indexPath) as? TopGenresCollectionViewCell else{
                return UICollectionViewCell()
        }
        
        let placeholderImage = UIImage(named: "ic_blur")
        var temp: [Song] = []
        
        switch indexPath.row {
        case 0:
            cell.imgCover.image = UIImage(named: "ic_pop")
            temp = listPop
        case 1:
            cell.imgCover.image = UIImage(named: "ic_rock")
            temp = listRock
        case 2:
            temp = listEdm
            cell.imgCover.image = UIImage(named: "ic_edm")
        case 3:
            temp = listChillout
            cell.imgCover.image = UIImage(named: "ic_chillout")
        case 4:
            temp = listClassic
            cell.imgCover.image = UIImage(named: "ic_classical")
        default:
            temp = listPop
            cell.imgCover.image = UIImage(named: "ic_pop")
        }
        
        if temp.count>0 {
            cell.img1?.sd_setImage(with: URL(string:temp[0].image),placeholderImage:placeholderImage)
            cell.img2?.sd_setImage(with: URL(string:temp[1].image),placeholderImage:placeholderImage)
            cell.img3?.sd_setImage(with: URL(string:temp[2].image),placeholderImage:placeholderImage)
            cell.img4?.sd_setImage(with: URL(string:temp[3].image),placeholderImage:placeholderImage)
            cell.img5?.sd_setImage(with: URL(string:temp[4].image),placeholderImage:placeholderImage)
            
            cell.lblArtist1.text = temp[0].artist_name
            cell.lblArtist2.text = temp[1].artist_name
            cell.lblArtist3.text = temp[2].artist_name
            cell.lblArtist4.text = temp[3].artist_name
            cell.lblArtist5.text = temp[4].artist_name
            
            cell.lblName1.text = temp[0].name
            cell.lblName2.text = temp[1].name
            cell.lblName3.text = temp[2].name
            cell.lblName4.text = temp[3].name
            cell.lblName5.text = temp[4].name
            
            cell.lblDuration1.text = Utils().getDurationText(duration: temp[0].duration)
            cell.lblDuration2.text = Utils().getDurationText(duration: temp[1].duration)
            cell.lblDuration3.text = Utils().getDurationText(duration: temp[2].duration)
            cell.lblDuration4.text = Utils().getDurationText(duration: temp[3].duration)
            cell.lblDuration5.text = Utils().getDurationText(duration: temp[4].duration)
            
            let tapGestureRecognizer1 = HomeTopGenresTapGestureRecognizer(target: self, action: #selector(tap(_:)))
            tapGestureRecognizer1.tapedSong = temp[0]
            tapGestureRecognizer1.songList = temp
            let tapGestureRecognizer2 = HomeTopGenresTapGestureRecognizer(target: self, action: #selector(tap(_:)))
            tapGestureRecognizer2.tapedSong = temp[1]
            tapGestureRecognizer2.songList = temp
            let tapGestureRecognizer3 = HomeTopGenresTapGestureRecognizer(target: self, action: #selector(tap(_:)))
            tapGestureRecognizer3.tapedSong = temp[2]
            tapGestureRecognizer3.songList = temp
            let tapGestureRecognizer4 = HomeTopGenresTapGestureRecognizer(target: self, action: #selector(tap(_:)))
            tapGestureRecognizer4.tapedSong = temp[3]
            tapGestureRecognizer4.songList = temp
            let tapGestureRecognizer5 = HomeTopGenresTapGestureRecognizer(target: self, action: #selector(tap(_:)))
            tapGestureRecognizer5.tapedSong = temp[4]
            tapGestureRecognizer5.songList = temp

            cell.v1.isUserInteractionEnabled = true
            cell.v1.tag = Int(temp[0].id)!
            cell.v1.addGestureRecognizer(tapGestureRecognizer1)
            
            cell.v2.isUserInteractionEnabled = true
            cell.v2.tag = Int(temp[1].id)!
            cell.v2.addGestureRecognizer(tapGestureRecognizer2)
            
            cell.v3.isUserInteractionEnabled = true
            cell.v3.tag = Int(temp[2].id)!
            cell.v3.addGestureRecognizer(tapGestureRecognizer3)
            
            cell.v4.isUserInteractionEnabled = true
            cell.v4.tag = Int(temp[3].id)!
            cell.v4.addGestureRecognizer(tapGestureRecognizer4)
            
            cell.v5.isUserInteractionEnabled = true
            cell.v5.tag = Int(temp[4].id)!
            cell.v5.addGestureRecognizer(tapGestureRecognizer5)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:  305, height: 450)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //        delegate?.didImageSelect(index: list[indexPath.row])
    }
}

class HomeTopGenresTapGestureRecognizer: UITapGestureRecognizer {
    var tapedSong: Song?
    var songList: [Song]?
}
