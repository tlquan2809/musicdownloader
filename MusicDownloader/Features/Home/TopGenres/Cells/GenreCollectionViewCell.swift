//
//  GenreCollectionViewCell.swift
//  MusicDownloader
//
//  Created by Quan Tran on 10/1/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class GenreCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var genreImage: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
