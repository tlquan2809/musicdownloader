//
//  TopGenresCollectionViewCell.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/24/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class TopGenresCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCover: UIImageView!
    
    @IBOutlet weak var v1: UIView!
    @IBOutlet weak var v2: UIView!
    @IBOutlet weak var v3: UIView!
    @IBOutlet weak var v4: UIView!
    @IBOutlet weak var v5: UIView!
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var lblName1: UILabel!
    @IBOutlet weak var lblArtist1: UILabel!
    @IBOutlet weak var lblDuration1: UILabel!
    
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var lblName2: UILabel!
    @IBOutlet weak var lblArtist2: UILabel!
    @IBOutlet weak var lblDuration2: UILabel!
    
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var lblName3: UILabel!
    @IBOutlet weak var lblArtist3: UILabel!
    @IBOutlet weak var lblDuration3: UILabel!
    
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var lblName4: UILabel!
    @IBOutlet weak var lblArtist4: UILabel!
    @IBOutlet weak var lblDuration4: UILabel!
    
    
    @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var lblName5: UILabel!
    @IBOutlet weak var lblArtist5: UILabel!
    @IBOutlet weak var lblDuration5: UILabel!
    
    
    var list: [Song] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgCover.roundCorners([.topLeft, .topRight], radius: 12)
        v5.roundCorners([.bottomLeft, .bottomRight], radius: 12)
     
    }
    
    
    
}
