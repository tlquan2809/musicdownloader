//
//  GenresViewController.swift
//  MusicDownloader
//
//  Created by Quan Tran on 10/1/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class GenresViewController: UIViewController {

    @IBOutlet weak var songTable: UITableView!
    
    var genre: Genres = .pop
    
    var songList: [Song] = []
    var tableDelegate = SongTableDelegate()
    var tableDataSource = SongTableDataSouce()
    var offset: Int = 25
    var cellID: String = "SongTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTable()
    }
    
    private func setupTable() {
        // TODO: Fix swipe
        tableDataSource.songList = songList
        tableDelegate.songList = songList
        tableDataSource.swipeActions = [.favorite, .addPlaylist]
        tableDelegate.willLoadMore = true
        tableDelegate.loadMoreCallback = { [weak self] in
            self?.getGenres(self?.genre ?? .pop, completion: { songs in
                if songs.count > 0 {
                    self?.songList.append(contentsOf: songs)
                    self?.tableDataSource.songList = self?.songList ?? []
                    self?.tableDelegate.songList = self?.songList ?? []
                    self?.songTable.reloadData()
                } else {
                    self?.tableDelegate.willLoadMore = false
                }
            })
        }
        songTable.delegate = tableDelegate
        tableDelegate.didSelectCellCallback = { song, songList in
            didSelectCell(song: song, songList: songList, parentVC: self)
        }
        songTable.dataSource = tableDataSource
        songTable.register(UINib(nibName: cellID, bundle: nil), forCellReuseIdentifier: cellID)
    }
    
    private func getGenres(_ genre: Genres, completion: (([Song]) -> Void)?) {
        APIClient.shared.getGenres(genre, offset: offset, completion: { [weak self] response in
            self?.offset += response.headers.results_count
            completion?(response.results)
            }, errorHandler: nil)
    }
}

extension GenresViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: nil, accessibilityLabel: nil, image: genre.icon, highlightedImage: genre.icon, userInfo: nil)
    }
}
