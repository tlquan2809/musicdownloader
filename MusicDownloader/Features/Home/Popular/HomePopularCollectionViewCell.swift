//
//  HomePopularCollectionViewCell.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/22/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift

class HomePopularCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var imgView: UIImageView!
   
    @IBOutlet weak var songName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        gradientView.roundCorners([.bottomLeft, .bottomRight], radius: 12)
        bgView.roundCorners([.topLeft, .topRight], radius: 12)
//        bgView.layer.cornerRadius = 12
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.gradientView.bounds
        gradientLayer.colors = [UIColor("#4D61FF").cgColor,UIColor("#FDA2FF").cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.gradientView.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         self.layer.mask = mask
    }
}
