//
//  PopularTableViewCell.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/22/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import SDWebImage

class PopularTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var list: [Song] = []
    var delegate: SongClickDelegate?
    let cellID = "HomePopularCollectionViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: cellID, bundle: nil), forCellWithReuseIdentifier: cellID)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension PopularTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as? HomePopularCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        let data = list[indexPath.row]
        let placeholderImage = UIImage(named: "ic_blur")!
        cell.imgView?.sd_setImage(with: URL(string: data.image),placeholderImage:placeholderImage)
        
        cell.artistName.text = data.artist_name
        cell.songName.text = data.name
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 335, height: 253)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        AVPlayerSingleton.shared.songList = list
        delegate?.didSongSelect(song: list[indexPath.row], songList: list)
    }
}

