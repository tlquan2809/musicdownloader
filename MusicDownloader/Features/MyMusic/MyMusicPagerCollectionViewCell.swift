//
//  MyMusicPagerCollectionViewCell.swift
//  MusicDownloader
//
//  Created by Quan Tran on 10/1/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class MyMusicPagerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var selectedBarView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func makeBarGradient() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = selectedBarView.bounds
        gradientLayer.colors = [UIColor(hexString: "#FDA2FF").cgColor, UIColor(hexString: "#4D61FF").cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        selectedBarView.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func removeGradient() {
        if let sublayers = selectedBarView.layer.sublayers, sublayers.count > 0 {
            selectedBarView.layer.sublayers?.remove(at: 0)
        }
    }
}
