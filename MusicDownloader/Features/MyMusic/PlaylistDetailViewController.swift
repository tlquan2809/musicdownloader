//
//  WorkingSongsViewController.swift
//  MusicDownloader
//
//  Created by Tung Nguyen on 9/30/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

class PlaylistDetailViewController: UIViewController {

    @IBOutlet weak var playlistNameLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var deleteButton: UIButton!
    
    // MARK: -- Properties
    
    var playlist = Playlist()
    var songList: [Song] = []
    var tableDelegate = SongTableDelegate()
    var tableDataSource = SongTableDataSouce()
    var cellID: String = "SongTableViewCell"
    var initialTouchPoint: CGPoint = CGPoint(x: 0, y: 0)
    var appDelegate: AppDelegate!
    weak var delegate: SongClickDelegate?
    // MARK: -- Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        delegate = self
        self.setupUI()
        self.setupTable()
    }
    
    // MARK: -- Function
    
    private func setupUI() {
        self.view.backgroundColor = UIColor.black
        
        self.playlistNameLabel.text = self.playlist.name
        self.playlistNameLabel.textColor = UIColor.white
        
        self.playButton.setImage(UIImage(named:"ic_play_border"), for: .normal)
        self.shareButton.setImage(UIImage(named: "ic_share"), for: .normal)
        
        self.deleteButton.setTitle("Delete this playlist", for: .normal)
        self.deleteButton.setTitleColor(UIColor.red, for: .normal)
    }
    
    private func setupTable() {
        tableView.backgroundColor = UIColor.black
        
        tableDataSource.songList = songList
        tableDelegate.songList = songList
        tableDataSource.swipeActions = [.favorite, .addPlaylist]
        tableDelegate.willLoadMore = true
        tableDelegate.didSelectCellCallback = { song, songList in
            didSelectCell(song: song, songList: songList, parentVC: self)
        }

        tableView.delegate = tableDelegate
        tableView.dataSource = tableDataSource
        tableView.register(UINib(nibName: cellID, bundle: nil), forCellReuseIdentifier: cellID)
    }
    
    // MARK: -- Actions
    
    @IBAction func deleteButtonTapped(_ sender: Any) {
        AlertManager.shared.show(title: "Warning!", message: "Are you sure you want to delete this playlist?", preferredStyle: .alert, numberOfOption: .two, parentView: self, okButtonDidTap: {
            RealmManager.shared.delete(obj: self.playlist)
            self.navigationController?.popViewController(animated: true)
        }, cancelButtonDidTap: {
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    @IBAction func playAllButtonDidTap(_ sender: UIButton) {
        delegate?.didSongSelect(song: songList.first!, songList: songList)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PlaylistDetailViewController: SongClickDelegate {
    func didSongSelect(song: Song, songList: [Song]) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        guard let playerView = storyBoard.instantiateViewController(withIdentifier: "PlayerViewViewController") as? PlayerViewViewController else {
            return
        }
        
        AVPlayerSingleton.shared.songList = songList
        _ = AVPlayerSingleton.shared.initPlayer(withSong: song)
        AVPlayerSingleton.shared.player?.play()
        playerView.modalPresentationStyle = .overCurrentContext
        appDelegate.setCurrentSong(song: song)
        DataManager.shared.isPlaying = true
        NotificationCenter.default.post(name: NSNotification.Name("removeMiniPlayerView"), object: nil)
        self.present(playerView, animated: true, completion: nil)
    }
}
