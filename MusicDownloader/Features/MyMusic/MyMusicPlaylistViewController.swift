//
//  MyMusicPlaylistViewController.swift
//  MusicDownloader
//
//  Created by AnhDT on 9/30/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import XLPagerTabStrip

protocol PlaylistDelegate: AnyObject {
    func reloadData()
}

class MyMusicPlaylistViewController: UIViewController {
    
    @IBOutlet weak var playlistTableView: UITableView!
    @IBOutlet weak var creatNewPlaylistButton: UIButton!
    
    var list: [Song] = []
    var playList: [Playlist] = []
    var tabType: MyMusicTab = .Playlist
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playlistTableView.separatorStyle = .none
        playlistTableView.backgroundColor = UIColor.black
        
        playlistTableView.dataSource = self
        playlistTableView.delegate = self
        
        playlistTableView.register(UINib(nibName: "TopDownloadTableViewCell", bundle: nil), forCellReuseIdentifier: "TopDownloadTableViewCell")
        view.backgroundColor = .black
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        playList = RealmManager.shared.getAllPlaylist().compactMap({ $0 })
        playlistTableView.reloadData()
    }
    
    @IBAction func createLlistTapped(_ sender: Any) {
        if !DataManager.shared.isShowingCreateNewPlaylist {
            let view = CreateNewPlaylistPopover()
            view.delegate = self
            view.show(in: self.view)
        }
    }
}

extension MyMusicPlaylistViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TopDownloadTableViewCell", for: indexPath) as? TopDownloadTableViewCell else {
            return UITableViewCell()
        }
        
        let pl = playList[indexPath.row]
        let songs = RealmManager.shared.getAllSongInPlaylist(name: pl.name).compactMap({ $0 })
        
        cell.titleforCell.text = pl.name
        cell.list = songs
        cell.collectionView.reloadData()
        cell.moreCallback = { [weak self] in
            let playlistDetailVC = PlaylistDetailViewController(nibName: "PlaylistDetailViewController", bundle: nil)
            playlistDetailVC.playlist = pl
            playlistDetailVC.songList = songs
            playlistDetailVC.modalPresentationStyle = .overFullScreen
            self?.navigationController?.pushViewController(playlistDetailVC, animated: true)
        }
        
        if pl.songs.count > 0 {
            cell.loadMoreLabel.textColor = UIColor(red: 253, green: 162, blue: 255)
            cell.directMoreImage.isHidden = false
            cell.loadMoreLabel.text = "See all"
        } else {
            cell.loadMoreLabel.textColor = .gray
            cell.directMoreImage.isHidden = true
            cell.loadMoreLabel.text = "Still Empty"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return playList[indexPath.row].songs.count > 0 ? 245 : 55
    }
}

extension MyMusicPlaylistViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: tabType.rawValue)
    }
}

extension MyMusicPlaylistViewController: PlaylistDelegate {
    func reloadData() {
        playList = RealmManager.shared.getAllPlaylist().compactMap({ $0 })
        self.playlistTableView.reloadData()
    }
}
