//
//  MyMusicViewController.swift
//  MusicDownloader
//
//  Created by Quan Tran on 10/1/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MyMusicViewController: BaseButtonBarPagerTabStripViewController<MyMusicPagerCollectionViewCell> {

    @IBOutlet weak var searchView: DesignableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        setupPager()
        super.viewDidLoad()
        
        setupGesture()
    }
    
    private func setupPager() {
        buttonBarItemSpec = ButtonBarItemSpec.nibFile(nibName: "MyMusicPagerCollectionViewCell", bundle: nil, width: { _ in
            return 40
        })
        
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = .clear
        settings.style.selectedBarHeight = 0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        
        changeCurrentIndexProgressive = { (oldCell: MyMusicPagerCollectionViewCell?, newCell: MyMusicPagerCollectionViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.titleLabel.textColor = UIColor.white.withAlphaComponent(0.3)
            oldCell?.removeGradient()
            
            newCell?.titleLabel.textColor = UIColor.white.withAlphaComponent(1)
            newCell?.makeBarGradient()
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let vc1 = MyMusicPlaylistViewController.loadFromNib()
        vc1.tabType = .Playlist
        
        let vc2 = MyMusicCommonViewController.loadFromNib()
        vc2.loadSongs = loadFavoriteSongs
        vc2.tabType = .Favorites
        
        let vc3 = MyMusicCommonViewController.loadFromNib()
        vc3.loadSongs = loadHistorySongs
        vc3.tabType = .History
        
        return [vc1, vc2, vc3]
    }
    
    private func loadFavoriteSongs() -> [Song] {
        let results = RealmManager.shared.getFavoriteSongs()
        return results.compactMap({ $0 })
    }
    
    private func loadHistorySongs() -> [Song] {
        let results = RealmManager.shared.getHistory()
        return results.compactMap({ $0 })
    }
    
    override func configure(cell: MyMusicPagerCollectionViewCell, for indicatorInfo: IndicatorInfo) {
        cell.titleLabel.text = indicatorInfo.title
    }
    
    private func setupGesture() {
        let searchTap = UITapGestureRecognizer(target: self, action: #selector(switchToSearch))
        searchView.addGestureRecognizer(searchTap)
    }
    
    @objc private func switchToSearch() {
        tabBarController?.selectedIndex = 1
    }
}
