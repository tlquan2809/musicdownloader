//
//  MyMusicCommonViewController.swift
//  MusicDownloader
//
//  Created by Quan Tran on 10/2/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import XLPagerTabStrip

enum MyMusicTab: String {
    case Playlist, Favorites, History
}

class MyMusicCommonViewController: UIViewController {

    @IBOutlet weak var songTable: UITableView!
    
    var songList: [Song] = []
    var tableDelegate = SongTableDelegate()
    var tableDataSource = SongTableDataSouce()
    var cellID: String = "SongTableViewCell"
    var tabType: MyMusicTab = .Playlist
    var loadSongs: (() -> [Song])?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let songs = loadSongs?() else {
            return
        }
        
        if songList != songs {
            songList = songs
            tableDataSource.songList = songList
            tableDelegate.songList = songList
            songTable.reloadData()
        }
    }
    
    private func setupTable() {
        switch tabType {
        case .Favorites:
            tableDataSource.swipeActions = [.favorite, .addPlaylist]
            tableDataSource.favoriteCallback = { [weak self] index in
                self?.songList.remove(at: index)
                self?.tableDataSource.songList.remove(at: index)
                self?.tableDelegate.songList.remove(at: index)
            }
        case .History:
            tableDataSource.swipeActions = [.favorite, .addPlaylist]
        case .Playlist:
            break
        }
        
        tableDataSource.songList = songList
        tableDelegate.songList = songList
        tableDelegate.willLoadMore = false
        tableDelegate.didSelectCellCallback = { song, songList in
            didSelectCell(song: song, songList: songList, parentVC: self)
        }
        songTable.delegate = tableDelegate
        songTable.dataSource = tableDataSource
        songTable.register(UINib(nibName: cellID, bundle: nil), forCellReuseIdentifier: cellID)
    }
}

extension MyMusicCommonViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: tabType.rawValue)
    }
}
