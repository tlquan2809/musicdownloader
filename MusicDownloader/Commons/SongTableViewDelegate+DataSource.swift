//
//  SongTableViewDelegate+DataSource.swift
//  MusicDownloader
//
//  Created by Quan Tran on 9/30/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import Foundation
import UIKit
import SwipeCellKit

enum SongCellAction {
    case delete, addPlaylist, favorite
}

class SongTableDataSouce: NSObject, UITableViewDataSource {
    var songList: [Song] = []
    var swipeActions: [SongCellAction] = []
    let cellID = "SongTableViewCell"
    var deleteCallback: ((Int) -> Void)?
    var favoriteCallback: ((Int) -> Void)?
    
    var swipeDelegate = SongTableSwipeDelegateImp()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as? SongTableViewCell else {
            return UITableViewCell()
        }
        
        let song = songList[indexPath.row]
        swipeDelegate.songList = songList
        swipeDelegate.actions = swipeActions
        swipeDelegate.deleteCallback = deleteCallback
        swipeDelegate.favoriteCallback = favoriteCallback
        cell.delegate = swipeActions.count > 0 ? swipeDelegate : nil
        cell.imgView.sd_setImage(with: URL(string: song.image), completed: nil)
        cell.lblName.text = song.name
        cell.lblArtist.text = song.artist_name
        cell.moreCallback = { 
            cell.showSwipe(orientation: .right)
        }
        
        return cell
    }
}

class SongTableDelegate: NSObject, UITableViewDelegate {
    var songList: [Song] = []
    var willLoadMore: Bool = false
    var loadMoreCallback: (() -> Void)?
    var didSelectCellCallback: ((Song, [Song]) -> ())?
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 82
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 82
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !willLoadMore {
            return
        }
        
        if indexPath.row < tableView.numberOfRows(inSection: 0) - 1 {
            return
        }
        
        loadMoreCallback?()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !songList.isEmpty {
            didSelectCellCallback?(songList[indexPath.row], songList)
        }
    }
}

class SongTableSwipeDelegateImp: SwipeTableViewCellDelegate {
    var actions: [SongCellAction] = [.addPlaylist, .favorite]
    var songList: [Song] = []
    var deleteCallback: ((Int) -> Void)?
    var favoriteCallback: ((Int) -> Void)?
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right && actions.count > 0 else { return nil }
        
        var actionList: [SwipeAction] = []
        let song = songList[indexPath.row]
        actions.forEach({ act in
            switch act {
            case .delete:
                let deleteAction = SwipeAction(style: .default, title: nil) { [weak self] action, indexPath in
                    DataManager.shared.deleteFile(at: song.localUrl)
                    RealmManager.shared.delete(song: song)
                    self?.deleteCallback?(indexPath.row)
                    tableView.reloadData()
                }
                deleteAction.image = UIImage(named: "cell_delete")
                deleteAction.backgroundColor = .clear
                actionList.append(deleteAction)
            case .addPlaylist:
                let addAction = SwipeAction(style: .default, title: nil) { action, indexPath in
                    let addToPlaylist = AddPlayListPopover()
                    addToPlaylist.currentSong = song
                    addToPlaylist.show()
                }
                addAction.image = UIImage(named: "cell_add")
                addAction.backgroundColor = .clear
                actionList.append(addAction)
            case .favorite:
                let favoriteAction = SwipeAction(style: .default, title: nil) { [weak self] action, indexPath in
                    RealmManager.shared.update(song: song, isFavorited: !song.isFavorited)
                    if let favorCallback = self?.favoriteCallback {
                        favorCallback(indexPath.row)
                        tableView.reloadData()
                    } else {
                        tableView.reloadRows(at: [indexPath], with: .none)
                    }
                }
                favoriteAction.image = UIImage(named: song.isFavorited ? "cell_favorite" : "cell_nonFavorite")
                favoriteAction.backgroundColor = .clear
                actionList.append(favoriteAction)
            }
        })
        
        return actionList
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var option = SwipeOptions()
        option.maximumButtonWidth = 70
        option.minimumButtonWidth = 70
        option.backgroundColor = .clear
        
        return option
    }
}
