//
//  SongTableViewCell.swift
//  MusicDownloader
//
//  Created by son nguyen on 9/27/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
import SwipeCellKit

class SongTableViewCell: SwipeTableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblArtist: UILabel!
    
    var moreCallback: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func moreAction(_ sender: UIButton) {
        moreCallback?()
    }
}
