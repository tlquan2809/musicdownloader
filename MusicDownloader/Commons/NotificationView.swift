//
//  NotificationView.swift
//  MusicDownloader
//
//  Created by Dung Nguyen on 10/10/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit
enum NotificationType {
    case noSong
    case downloading
    case downloadSucces
    case downloadError
    case unknowError
    case duplicateError
    case lostConnection
    case songIsDownloading
}

class NotificationView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var notiImageView: UIImageView!
    
    var songName: String = ""
    var isShowing = false
    var notiType: NotificationType = .downloading
    var downloadingString = "Downloading..."
    var downloadErrorString = "Can not download!"
    var unknowErrorString = "Some thing went wrong!"
    var duplicateErrorString = "You already have this song!"
    var lostConnectionString = "Lost internet connection!"
    var songIsDownloading = "This song is downloading!"
    var noSongString = "No song is selected!"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("NotificationView", owner: self, options: nil)
        addSubview(contentView)
        contentView.layer.cornerRadius = 8
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        notiImageView.layer.cornerRadius = notiImageView.bounds.height / 2
    }
    
    private func setupUI() {
        switch notiType {
        case .downloading:
            descriptionLabel.text = downloadingString
            notiImageView.image = UIImage(named: "ic_down")
        case .downloadSucces:
            descriptionLabel.text = "Download \(songName) successfully."
            notiImageView.image = UIImage(named: "ic_success")
        case .downloadError:
            descriptionLabel.text = downloadErrorString
            notiImageView.image = UIImage(named: "ic_error")
        case .unknowError:
            descriptionLabel.text = unknowErrorString
            notiImageView.image = UIImage(named: "ic_error")
        case .duplicateError:
            descriptionLabel.text = duplicateErrorString
            notiImageView.image = UIImage(named: "ic_error")
        case .lostConnection:
            descriptionLabel.text = lostConnectionString
            notiImageView.image = UIImage(named: "ic_error")
        case .songIsDownloading:
            descriptionLabel.text = songIsDownloading
            notiImageView.image = UIImage(named: "ic_error")
        case .noSong:
            descriptionLabel.text = noSongString
            notiImageView.image = UIImage(named: "ic_error")
        }
    }
    
    func show(in view: UIView) {
        setupUI()
        view.addSubview(self)
        view.bringSubviewToFront(self)
        self.frame = CGRect(x: UIScreen.main.bounds.width / 2 - 167.5, y: -contentView.bounds.height - 10, width: 335, height: 60)
        contentView.alpha = 0
        UIView.animate(withDuration: 0.2) {
            self.contentView.alpha = 1
            self.center = CGPoint(x: view.center.x, y: self.contentView.bounds.height + 10)
        }
        setupTimer()
    }
    
    func hide() {
        UIView.animate(withDuration: 0.2, animations: {
            self.contentView.alpha = 1
            self.center.y = -self.contentView.bounds.height - 10
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }
    
    private func setupTimer() {
        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: true) { timer in
            self.hide()
        }
    }
}
