//
//  ExString.swift
//  MusicDownloader
//
//  Created by Quan Tran on 9/29/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import Foundation

extension String {
    func removeExtraSpaces() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: "[\\s\n]+", with: " ", options: .regularExpression, range: nil)
    }
}
