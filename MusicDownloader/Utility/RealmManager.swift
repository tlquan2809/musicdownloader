//
//  RealmManager.swift
//  MusicDownloader
//
//  Created by Quan Tran on 10/3/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager {
    static let shared = RealmManager()
    private init() {}
    
    let realm = try! Realm()
    
    func save(song: Song) {
        try! realm.write {
            realm.add(song, update: .modified)
        }
    }
    
    func save(playlist: Playlist) {
        try! realm.write {
            realm.add(playlist, update: .modified)
        }
    }
    
    func saveAfterDownload(song: Song, url: String) {
        try! realm.write {
            song.localUrl = url
            realm.add(song, update: .modified)
        }
    }
    
    func saveToHistory(song: Song) {
        if let localSong = getSongByID(song.id), localSong.history {
            return
        }
        
        try! realm.write {
            song.history = true
            realm.add(song, update: .modified)
        }
    }
    
    func getSongByID(_ id: String) -> Song? {
        return realm.objects(Song.self).filter("id = '\(id)'").first
    }
    
    func getFavoriteSongs() -> Results<Song> {
        return realm.objects(Song.self).filter("isFavorited = 1")
    }
    
    func getDownloadedSongs() -> Results<Song> {
        return realm.objects(Song.self).filter("localUrl != ''")
    }
    
    func getPlaylistByName(_ name: String) -> Playlist? {
        return realm.objects(Playlist.self).filter("name = '\(name)'").first
    }
    
    func getAllPlaylist() -> Results<Playlist> {
        return realm.objects(Playlist.self).sorted(byKeyPath: "name")
    }
    
    func getHistory() -> Results<Song> {
        return realm.objects(Song.self).filter("history = 1")
    }
    
    func getAllSongInPlaylist(name: String) -> [Song] {
        guard let playList = realm.objects(Playlist.self).filter("name = '\(name)'").first else {
            return []
        }
        
        var songs: [Song] = []
        playList.songs.forEach({ songID in
            if let song = getSongByID(songID)  {
                songs.append(song)
            }
        })
        return songs
    }
    
    func update(song: Song, isFavorited: Bool, shouldSave: Bool = true) {
        try! realm.write {
            song.isFavorited = isFavorited
            if shouldSave {
                realm.add(song, update: .modified)
            }
        }
    }
    
    func delete(song: Song) {
        try! realm.write {
            song.localUrl = ""
            realm.add(song, update: .modified)
        }
    }
    
    func update(playlist: Playlist, with song: Song) {
        if playlist.songs.contains(song.id) {
            return
        }
        
        try! realm.write {
            playlist.songs.append(song.id)
            realm.add(playlist, update: .modified)
        }
    }
    
    func delete(obj: Object) {
        try! realm.write {
            realm.delete(obj)
        }
    }
}
