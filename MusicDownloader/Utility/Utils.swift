//
//  Utils.swift
//  MusicDownloader
//
//  Created by son nguyen on 8/24/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import Foundation
import Alamofire

class Utils {
    func getDurationText(duration : Int? ) -> String{
        var result = ""
        
        if duration != nil {
            let min = duration! / 60
            let sec = duration! % 60
            
            if min >= 10 {
                result += "\(min)"
            } else {
                result += "0\(min)"
            }
            result += ":"
            
            if sec >= 10 {
                result += "\(sec)"
            } else {
                result += "0\(sec)"
            }
        }
        
        return result
    }
    
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
