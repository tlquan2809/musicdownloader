//
//  Delegate.swift
//  MusicDownloader
//
//  Created by son nguyen on 9/6/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

protocol SongClickDelegate: AnyObject {
    func didSongSelect(song: Song, songList: [Song])
}

func didSelectCell(song: Song, songList: [Song], parentVC: UIViewController) {
    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
    guard let playerView = storyBoard.instantiateViewController(withIdentifier: "PlayerViewViewController") as? PlayerViewViewController,
        let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }

    AVPlayerSingleton.shared.songList = songList
    _ = AVPlayerSingleton.shared.initPlayer(withSong: song)
    AVPlayerSingleton.shared.player?.play()
    playerView.modalPresentationStyle = .overCurrentContext
    appDelegate.setCurrentSong(song: song)
    DataManager.shared.isPlaying = true
    NotificationCenter.default.post(name: NSNotification.Name("removeMiniPlayerView"), object: nil)
    
    parentVC.present(playerView, animated: true, completion: nil)
}
