//
//  AlertManager.swift
//  MusicDownloader
//
//  Created by Dung Nguyen on 10/4/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import UIKit

enum NumberOfOption {
    case one
    case two
}

class AlertManager {
    static let shared = AlertManager()
    
    func show(title: String,
              message: String,
              preferredStyle: UIAlertController.Style,
              numberOfOption: NumberOfOption,
              parentView: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        
        switch numberOfOption {
        case .one:
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        case .two:
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        }
        
        parentView.present(alert, animated: true)
    }
    
    func show(title: String,
              message: String,
              preferredStyle: UIAlertController.Style,
              numberOfOption: NumberOfOption,
              parentView: UIViewController,
              okButtonDidTap: (() -> ())?,
              cancelButtonDidTap: (() -> ())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        
        switch numberOfOption {
        case .one:
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                cancelButtonDidTap?()
            }))
        case .two:
            alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
                okButtonDidTap?()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                cancelButtonDidTap?()
            }))
        }
        
        parentView.present(alert, animated: true)
    }
}

func showNotification(notiType: NotificationType, songName: String? = nil) {
    let noti = NotificationView()
    noti.notiType = notiType
    if let songName = songName {
        noti.songName = songName
    }
    
    guard let topVC = getTopMostViewController() else { return }
    noti.show(in: topVC.view)
}

func getTopMostViewController() -> UIViewController? {
    if var topController = UIApplication.shared.keyWindow?.rootViewController {
        while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
        }
        
        if topController.isKind(of: UINavigationController.self) {
            if let firstVC = topController.children.first {
                topController = firstVC
            }
        }
        
        return topController
    }
    return nil
}
