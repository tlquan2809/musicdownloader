//
//  DataManager.swift
//  MusicDownloader
//
//  Created by Quan Tran on 9/29/20.
//  Copyright © 2020 son nguyen. All rights reserved.
//

import Foundation

class DataManager {
    static let shared = DataManager()
    private init() {}
    
    var token = ""
    var isShowingAddPlaylist: Bool = false
    var isShowingPlayFeature: Bool = false
    var isEnableShuffle: Bool = false
    var isPlaying: Bool = false
    var isShowingCreateNewPlaylist: Bool = false
    var isShowingAddPlaylistAgain: Bool = false
    var isDownloading: Bool = false
    
    func deleteFile(at filePath: String) {
        let fileManager = FileManager.default
        var docDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            docDir.appendPathComponent(filePath)
            try fileManager.removeItem(at: docDir)
        } catch {
            print("delete file failed")
        }
    }
}
